import 'dart:convert';

import 'package:ecommerce/data/error/exceptions.dart';
import 'package:ecommerce/data/network/network_status.dart';
import 'package:ecommerce/data/remote/repositories/abstract/remote_repository_wrapper_abstract.dart';
import 'package:ecommerce/data/remote/repositories/implementation/category_remote_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../fixtures/fixture_reader.dart';

class MockRemoteWrapper extends Mock
    implements RemoteRepositoryWrapperAbstract {}

class MockNetworkStatus extends Mock implements NetworkStatus {}

void main() {
  late MockRemoteWrapper remoteWrapper;
  late MockNetworkStatus mockNetworkStatus;
  late CategoryRemoteRepository remoteCategoryRepository;

  setUp(() {
    remoteWrapper = MockRemoteWrapper();
    mockNetworkStatus = MockNetworkStatus();
    remoteCategoryRepository = CategoryRemoteRepository(remoteWrapper);
  });

  void runTestsOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(mockNetworkStatus.isConnected).thenAnswer((_) async => true);
      });

      body();
    });
  }

  group("Get list of categories", () {
    runTestsOnline(() async {
      test('should return list of categories when getCategories is successful',
          () async {
        when(remoteWrapper.getCategoryList()).thenAnswer(
            (_) async => json.decode(fixture('data/categories.json')));

        final categories = await remoteCategoryRepository.getCategories();
        expect(categories.length, equals(2));
      });

      test('should return server failure when getCategories is unsuccessful',
          () async {
        when(remoteWrapper.getCategoryList()).thenThrow(HttpRequestException());

        expect(() => remoteCategoryRepository.getCategories(),
            throwsA(isInstanceOf<RemoteServerException>()));
      });
    });
  });
}
