import 'dart:convert';

import 'package:ecommerce/data/error/exceptions.dart';
import 'package:ecommerce/data/network/network_status.dart';
import 'package:ecommerce/data/remote/repositories/abstract/remote_repository_wrapper_abstract.dart';
import 'package:ecommerce/data/remote/repositories/implementation/product_remote_repository.dart';
import 'package:ecommerce/domain/usecases/products/products_by_filter_params.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../fixtures/fixture_reader.dart';

class MockRemoteWrapper extends Mock
    implements RemoteRepositoryWrapperAbstract {}

class MockNetworkStatus extends Mock implements NetworkStatus {}

void main() {
  late MockRemoteWrapper remoteWrapper;
  late MockNetworkStatus mockNetworkStatus;
  late ProductRemoteRepository remoteProductRepository;

  setUp(() {
    remoteWrapper = MockRemoteWrapper();
    mockNetworkStatus = MockNetworkStatus();
    remoteProductRepository = ProductRemoteRepository(remoteWrapper);
  });

  void runTestsOnline(Function body) {
    group("device is online", () {
      setUp(() {
        when(mockNetworkStatus.isConnected).thenAnswer((_) async => true);
      });

      body();
    });
  }

  group('Get list of products', () {
    runTestsOnline(() async {
      test('should return list of products when getProducts is successful',
          () async {
        when(remoteWrapper
                .getProductList(ProductsByFilterParams(categoryId: 0)))
            .thenAnswer(
                (_) async => json.decode(fixture('data/products.json')));

        final products = await remoteProductRepository.getProducts();

        expect(products.length, equals(10));
      });

      test('should return server failure when getProducts is unsuccessful',
          () async {
        when(remoteWrapper
                .getProductList(ProductsByFilterParams(categoryId: 0)))
            .thenThrow(HttpRequestException());

        expect(() => remoteProductRepository.getProducts(),
            throwsA(isInstanceOf<RemoteServerException>()));
      });
    });
  });
}
