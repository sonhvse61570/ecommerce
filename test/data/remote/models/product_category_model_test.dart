import 'dart:convert';

import 'package:ecommerce/data/remote/models/product_category_model.dart';
import 'package:ecommerce/domain/entities/product/product_category_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../fixtures/fixture_reader.dart';

void main() {
  const productCategoryModel = ProductCategoryModel(
      id: 18,
      title: 'Albums',
      description: 'The best music albums available online.',
      image: 'https://woocommerce.openflutterproject.com/wp-content/uploads/2020/03/cd_4_angle.jpg',
      thumb: 'https://woocommerce.openflutterproject.com/wp-content/uploads/2020/03/cd_4_angle.jpg',
      parentId: 16,
      orderNumber: 0,
      count: 4
  );

  group("extend Entity", () {
    test("should be a subclass of ProductCategoryEntity entity", () async {
      expect(productCategoryModel, isA<ProductCategoryEntity>());
    });
  });

  group('fromJson', () {
    test("should return a valid model", () async {
      final Map<String, dynamic> jsonMap = json.decode(fixture("data/category.json"));
      final result = ProductCategoryModel.fromJson(jsonMap);

      expect(result, productCategoryModel);
    });
  });

  group('toJson', () {
    test("should return a JSON map with proper data", () async {
      final result = productCategoryModel.toJson();

      final expectedMap = {
        'id':18,
        'name':'Albums',
        'parent':16,
        'description':'The best music albums available online.',
        'image':
        {
          'src':'https://woocommerce.openflutterproject.com/wp-content/uploads/2020/03/cd_4_angle.jpg',
        },
        'menu_order':0,
        'count':4
      };
      expect(result, expectedMap);
    });
  });
}