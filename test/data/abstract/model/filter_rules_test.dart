import 'dart:convert';

import 'package:ecommerce/data/model/filter_rules.dart';
import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/remote/repositories/abstract/remote_repository_wrapper_abstract.dart';
import 'package:ecommerce/data/remote/repositories/implementation/product_remote_repository.dart';
import 'package:ecommerce/domain/usecases/products/products_by_filter_params.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../fixtures/fixture_reader.dart';


class MockRemoteWrapper extends Mock
    implements RemoteRepositoryWrapperAbstract {}

void main() {
  late MockRemoteWrapper remoteWrapper;
  late ProductRemoteRepository remoteProductRepository;

  setUp(() {
    remoteWrapper = MockRemoteWrapper();
    remoteProductRepository = ProductRemoteRepository(remoteWrapper);
  });

  group("Generate selectable attributes from list of products", () {
    test(
        'should return lsit of categories when findProductsByFilterUserCase.execute is successful',
        () async {
      // arrange
      when(remoteWrapper.getProductList(ProductsByFilterParams(categoryId: 0)))
          .thenAnswer((_) async => json.decode(fixture('data/products.json')));

      // act
      List<Product> products = await remoteProductRepository.getProducts(categoryId: 1);
      FilterRules filterRules = FilterRules.getSelectableAttributes(products);

      // assert
      expect(filterRules.categories.length, equals(6));
      expect(filterRules.selectedPriceRange.maxPrice, equals(35));
      expect(filterRules.selectedPriceRange.minPrice, equals(0));
      expect(filterRules.selectableAttributes.length, equals(2));
      expect(filterRules.hashTags.length, equals(4));
    });
  });
}
