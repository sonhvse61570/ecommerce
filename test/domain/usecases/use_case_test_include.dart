import 'package:ecommerce/data/network/network_status.dart';
import 'package:ecommerce/data/remote/repositories/abstract/remote_repository_wrapper_abstract.dart';
import 'package:ecommerce/data/remote/repositories/implementation/category_remote_repository.dart';
import 'package:ecommerce/data/remote/repositories/implementation/product_remote_repository.dart';
import 'package:ecommerce/data/repositories/abstract/category_repository.dart';
import 'package:ecommerce/data/repositories/abstract/product_repository.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';

class MockRemoteWrapper extends Mock implements RemoteRepositoryWrapperAbstract {}

final sl = GetIt.instance;

void setupLocator() {
  sl.allowReassignment = true;

  late MockRemoteWrapper remote = MockRemoteWrapper();

  sl.registerLazySingleton<MockRemoteWrapper>(() => remote);

  sl.registerLazySingleton<RemoteRepositoryWrapperAbstract>(() => remote);

  sl.registerLazySingleton<CategoryRepository>(() => CategoryRemoteRepository(remote));

  sl.registerLazySingleton<ProductRepository>(() => ProductRemoteRepository(remote));

  sl.registerLazySingleton<NetworkStatus>(() => NetworkStatusImpl());
}