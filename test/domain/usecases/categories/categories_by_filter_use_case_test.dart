import 'dart:convert';

import 'package:ecommerce/data/error/exceptions.dart';
import 'package:ecommerce/domain/usecases/categories/find_categories_by_filter_use_case.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../fixtures/fixture_reader.dart';


import '../use_case_test_include.dart';

void main() {
  setupLocator();
  late MockRemoteWrapper remote;
  late FindCategoriesByFilterUseCase findCategoriesByFilterUseCase;
  late CategoriesByFilterParams categoriesByFilterParams;

  setUp(() {
    remote = sl();
    findCategoriesByFilterUseCase = FindCategoriesByFilterUseCaseImpl();
    categoriesByFilterParams = CategoriesByFilterParams(categoryId: 1);
  });

  group('Get list of categories', () {
    test(
        'should return list of categories when findCategoriesByFilterUseCase.execute is successful',
        () async {
      when(remote.getCategoryList(parentId: 1)).thenAnswer(
          (_) async => json.decode(fixture('data/categories.json')));

      final categoriesData =
          await findCategoriesByFilterUseCase.execute(categoriesByFilterParams);

      expect(categoriesData.categories.length, equals(2));
    });

    test(
        "should return server failure when findProductsByFilterUseCase.execute is unsuccessful",
        () async {
      when(remote.getCategoryList(parentId: 1))
          .thenThrow(HttpRequestException());

      final categoriesData =
          await findCategoriesByFilterUseCase.execute(categoriesByFilterParams);

      expect(categoriesData.validResults, equals(false));
      expect(
          categoriesData.exception, isInstanceOf<EmptyCategoriesException>());
    });
  });
}
