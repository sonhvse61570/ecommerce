import 'dart:convert';

import 'package:ecommerce/data/error/exceptions.dart';
import 'package:ecommerce/domain/usecases/products/find_products_by_filter_use_case.dart';
import 'package:ecommerce/domain/usecases/products/products_by_filter_params.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../../../fixtures/fixture_reader.dart';
import '../use_case_test_include.dart';

void main() {
  late FindProductsByFilterUseCase findProductsByFilterUseCase;
  late ProductsByFilterParams productsByFilterParams;

  setupLocator();

  MockRemoteWrapper remote = sl();

  setUp(() {
    findProductsByFilterUseCase = FindProductsByFilterUseCaseImpl();
    productsByFilterParams = ProductsByFilterParams(categoryId: 1);
  });

  group('Get list of products', () {
    test('should return list of categories when findProductsByFilterUseCase.execute is successful', () async {
      when(remote.getProductList(ProductsByFilterParams(categoryId: 0))).thenAnswer((_) async => jsonDecode(fixture('data/products.json')));

      final productsData = await findProductsByFilterUseCase.execute(productsByFilterParams);

      expect(productsData.products.length, equals(10));
    });

    test(
      'should return server failure when findProductsByFilterUseCase.execute is unsuccessful',
          () async {
        // arrange
        when(remote.getProductList(ProductsByFilterParams(categoryId: 0)))
            .thenThrow(HttpRequestException());
        // act
        final productsData = await findProductsByFilterUseCase.execute(productsByFilterParams);
        // assert
        expect(productsData.validResults, equals(false));
        expect(productsData.exception, isInstanceOf<EmptyProductsException>());
      },
    );
  });
}