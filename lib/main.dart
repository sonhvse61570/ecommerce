import 'package:ecommerce/config/app_routes.dart';
import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/repositories/abstract/cart_repository.dart';
import 'package:ecommerce/data/repositories/abstract/category_repository.dart';
import 'package:ecommerce/data/repositories/abstract/favorites_repository.dart';
import 'package:ecommerce/data/repositories/abstract/product_repository.dart';
import 'package:ecommerce/data/repositories/abstract/user_repository.dart';
import 'package:ecommerce/presentation/features/authentication/authentication.dart';
import 'package:ecommerce/presentation/features/cart/cart_screen.dart';
import 'package:ecommerce/presentation/features/categories/categories_screen.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_screen.dart';
import 'package:ecommerce/presentation/features/favorites/favorites_screen.dart';
import 'package:ecommerce/presentation/features/filters/filters_screen.dart';
import 'package:ecommerce/presentation/features/home/home_screen.dart';
import 'package:ecommerce/presentation/features/product_details/product_details_screen.dart';
import 'package:ecommerce/presentation/features/products/products_screen.dart';
import 'package:ecommerce/presentation/features/profile/profile_screen.dart';
import 'package:ecommerce/presentation/features/recover_password/recover_password_bloc.dart';
import 'package:ecommerce/presentation/features/recover_password/recover_password_screen.dart';
import 'package:ecommerce/presentation/features/sign_in/sign_in_bloc.dart';
import 'package:ecommerce/presentation/features/sign_in/sign_in_screen.dart';
import 'package:ecommerce/presentation/features/sign_up/sign_up_bloc.dart';
import 'package:ecommerce/presentation/features/sign_up/sign_up_screen.dart';
import 'package:ecommerce/presentation/features/splash_screen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_translate/flutter_translate.dart';

import 'locator.dart' as service_locator;

class SimpleBlocDelegate extends BlocObserver {
  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    if (kDebugMode) {
      print(transition);
    }
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
    if (kDebugMode) {
      print(error);
    }
  }
}

void main() async {
  service_locator.init();

  var delegate = await LocalizationDelegate.create(
    fallbackLocale: 'en_US',
    supportedLocales: ['en_US', 'de', 'fr'],
  );
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  Bloc.observer = SimpleBlocDelegate();
  runApp(
    BlocProvider<AuthenticationBloc>(
      create: (context) => AuthenticationBloc()..add(AppStarted()),
      child: MultiRepositoryProvider(
        providers: [
          RepositoryProvider<CategoryRepository>(
              create: (context) => service_locator.sl()),
          RepositoryProvider<ProductRepository>(
              create: (context) => service_locator.sl()),
          RepositoryProvider<FavoritesRepository>(
              create: (context) => service_locator.sl()),
          RepositoryProvider<UserRepository>(
              create: (context) => service_locator.sl()),
          RepositoryProvider<CartRepository>(
              create: (context) => service_locator.sl()),
        ],
        child: LocalizedApp(
          delegate,
          const App(),
        ),
      ),
    ),
  );
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var localizationDelegate = LocalizedApp.of(context).delegate;

    return LocalizationProvider(
      state: LocalizationProvider.of(context).state,
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          localizationDelegate,
        ],
        supportedLocales: localizationDelegate.supportedLocales,
        debugShowCheckedModeBanner: false,
        locale: localizationDelegate.currentLocale,
        title: 'Open Flutter E-commerce',
        theme: AppTheme.of(context),
        onGenerateRoute: _registerRoutesWithParameters,
        routes: _registerRoutes(),
      ),
    );
  }

  Route _registerRoutesWithParameters(RouteSettings settings) {
    switch (settings.name) {
      case AppRoutes.shop:
        return MaterialPageRoute(
          builder: (context) => const CategoriesScreen(),
          settings: settings,
        );
      case AppRoutes.productList:
        return MaterialPageRoute(
          builder: (context) => const ProductsScreen(),
          settings: settings,
        );
      case AppRoutes.product:
        return MaterialPageRoute(
          builder: (context) => const ProductDetailsScreen(),
          settings: settings,
        );
      case AppRoutes.filters:
        return MaterialPageRoute(
          builder: (context) => const FiltersScreen(),
          settings: settings,
        );
      default:
        return MaterialPageRoute(
          builder: (context) => const HomeScreen(),
          settings: settings,
        );
    }
  }

  Map<String, WidgetBuilder> _registerRoutes() => <String, WidgetBuilder>{
        AppRoutes.home: (context) => const HomeScreen(),
        AppRoutes.cart: (context) => const CartScreen(),
        AppRoutes.checkout: (context) => const CheckoutScreen(),
        AppRoutes.favourites: (context) => const FavoritesScreen(),
        AppRoutes.signIn: (context) => _buildSignInBloc(),
        AppRoutes.signUp: (context) => BlocProvider<SignUpBloc>(
              create: (context) => SignUpBloc(
                userRepository: RepositoryProvider.of<UserRepository>(context),
                authenticationBloc:
                    BlocProvider.of<AuthenticationBloc>(context),
              ),
              child: const SignUpScreen(),
            ),
        AppRoutes.forgotPassword: (context) =>
            BlocProvider<RecoverPasswordBloc>(
              create: (context) => RecoverPasswordBloc(
                  userRepository:
                      RepositoryProvider.of<UserRepository>(context)),
              child: const RecoverPasswordScreen(),
            ),
        AppRoutes.profile: (context) =>
            BlocBuilder<AuthenticationBloc, AuthenticationState>(
              builder: (context, state) {
                switch (state.runtimeType) {
                  case Authenticated:
                    return const ProfileScreen();
                  case Unauthenticated:
                    return _buildSignInBloc();
                  default:
                    return const SplashScreen();
                }
              },
            ),
      };

  BlocProvider<SignInBloc> _buildSignInBloc() {
    return BlocProvider<SignInBloc>(
      create: (context) => SignInBloc(
        userRepository: RepositoryProvider.of<UserRepository>(context),
        authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
      ),
      child: const SignInScreen(),
    );
  }
}
