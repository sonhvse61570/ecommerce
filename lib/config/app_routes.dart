class AppRoutes {
  static const home = '/';
  static const shop = 'shop';
  static const cart = 'cart';
  static const favourites = 'favourites';
  static const productList = 'product-list';
  static const product = 'product';
  static const profile = 'profile';
  static const checkout = 'checkout';
  static const signUp = 'sign-up';
  static const signIn = 'sign-in';
  static const forgotPassword = 'forgot-pass';
  static const filters = 'filters';
}