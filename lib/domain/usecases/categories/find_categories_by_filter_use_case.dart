import 'package:ecommerce/data/model/category.dart';
import 'package:ecommerce/data/repositories/abstract/category_repository.dart';
import 'package:ecommerce/domain/usecases/base_use_case.dart';
import 'package:ecommerce/locator.dart';

abstract class FindCategoriesByFilterUseCase
    implements
        BaseUseCase<CategoriesByFilterResult, CategoriesByFilterParams> {}

class FindCategoriesByFilterUseCaseImpl
    implements FindCategoriesByFilterUseCase {
  @override
  Future<CategoriesByFilterResult> execute(
      CategoriesByFilterParams params) async {
    try {
      CategoryRepository _categoryRepository = sl();
      List<ProductCategory> categories = await _categoryRepository
          .getCategories(parentCategoryId: params.categoryId);
      return CategoriesByFilterResult(
        categories: categories,
        quantity: categories.length,
        exception: null,
      );
    } catch (e) {
      return CategoriesByFilterResult(
        categories: [],
        exception: EmptyCategoriesException(),
        quantity: 0,
      );
    }
  }
}

class EmptyCategoriesException implements Exception {}

class CategoriesByFilterParams {
  final int categoryId;

  CategoriesByFilterParams({required this.categoryId});
}

class CategoriesByFilterResult extends UseCaseResult {
  final List<ProductCategory> categories;
  final int quantity;

  CategoriesByFilterResult({
    required this.categories,
    required this.quantity,
    required Exception? exception,
  }) : super(exception: exception, result: true);
}
