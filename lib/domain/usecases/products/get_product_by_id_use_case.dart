import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/repositories/abstract/product_repository.dart';
import 'package:ecommerce/domain/usecases/base_use_case.dart';
import 'package:ecommerce/locator.dart';

abstract class GetProductByIdUseCase
    implements BaseUseCase<ProductDetailsResults, ProductDetailsParams> {}

class GetProductByIdUseCaseImpl implements GetProductByIdUseCase {
  @override
  Future<ProductDetailsResults> execute(ProductDetailsParams params) async {
    //TODO: replace fetch from API
    //TEMP solution
    ProductRepository productRepository = sl();
    //TODO:
    List<Product> products = await productRepository.getProducts(
        categoryId: params.categoryId, filterRules: null);
    Product? product;
    for (var f in products) {
      if (f.id == params.productId) product = f;
    }
    return ProductDetailsResults(
        productDetails: product, similarProducts: products);
  }
}

class ProductDetailsResults {
  final Product? productDetails;
  final List<Product> similarProducts;

  ProductDetailsResults(
      {required this.productDetails, required this.similarProducts});
}

class ProductDetailsParams {
  final int productId;
  final int categoryId;

  ProductDetailsParams({required this.productId, required this.categoryId});
}
