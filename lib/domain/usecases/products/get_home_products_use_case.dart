import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/repositories/abstract/product_repository.dart';
import 'package:ecommerce/domain/usecases/base_use_case.dart';
import 'package:ecommerce/locator.dart';

abstract class GetHomePageProductsUseCase
    implements BaseUseCase<HomeProductsResult, HomeProductParams> {}

class GetHomePageProductsUseCaseImpl implements GetHomePageProductsUseCase {
  @override
  Future<HomeProductsResult> execute(HomeProductParams params) async {
    try {
      ProductRepository productRepository = sl();
      return HomeProductsResult(
        salesProducts: await productRepository.getProducts(
          categoryId: 1,
          filterRules: null,
        ),
        newProducts: await productRepository.getProducts(
          categoryId: 2,
          filterRules: null,
        ),
        result: false,
        exception: null,
      );
    } catch (e) {
      return HomeProductsResult(
        salesProducts: [],
        newProducts: [],
        result: false,
        exception: HomeProductsException(),
      );
    }
  }
}

class HomeProductParams {}

class HomeProductsResult extends UseCaseResult {
  final List<Product> salesProducts;
  final List<Product> newProducts;

  HomeProductsResult({
    required this.salesProducts,
    required this.newProducts,
    required Exception? exception,
    required bool result,
  }) : super(exception: exception, result: result);
}

class HomeProductsException implements Exception {}
