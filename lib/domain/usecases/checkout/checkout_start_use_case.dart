import 'package:ecommerce/data/model/cart_item.dart';
import 'package:ecommerce/data/model/payment_method.dart';
import 'package:ecommerce/data/model/shipping_address.dart';
import 'package:ecommerce/data/repositories/abstract/cart_repository.dart';
import 'package:ecommerce/data/repositories/abstract/payment_method_repository.dart';
import 'package:ecommerce/data/repositories/abstract/shipping_address_repository.dart';
import 'package:ecommerce/domain/usecases/base_use_case.dart';
import 'package:ecommerce/locator.dart';

abstract class CheckoutStartUseCase
    implements BaseUseCase<CheckoutStartResult, CheckoutStartParams> {}

class CheckoutStartUseCaseImpl implements CheckoutStartUseCase {
  @override
  Future<CheckoutStartResult> execute(CheckoutStartParams params) async {
    try {
      ShippingAddressRepository shippingAddressRepository = sl();
      PaymentMethodRepository paymentMethodRepository = sl();
      CartRepository cartRepository = sl();
      //TODO: make delivery price dynamic
      const double deliveryPrice = 10;
      final double calculatedPrice = cartRepository.getCalculatedPrice();
      return CheckoutStartResult(
        result: true,
        paymentMethods: await paymentMethodRepository.getPaymentMethodList(),
        shippingAddress:
            await shippingAddressRepository.getShippingAddressList(),
        cartItems: await cartRepository.getCartContent(),
        currentPaymentMethod:
            await paymentMethodRepository.getDefaultPaymentMethod(),
        currentShippingAddress:
            await shippingAddressRepository.getDefaultShippingAddress(),
        totalCalculatedPrice: calculatedPrice,
        deliveryPrice: deliveryPrice,
        summaryPrice: calculatedPrice + deliveryPrice,
        exception: null,
      );
    } catch (e) {
      return CheckoutStartResult(
          result: false,
          exception: CheckoutStartException(),
          currentPaymentMethod: null,
          deliveryPrice: null,
          totalCalculatedPrice: null,
          paymentMethods: [],
          shippingAddress: [],
          cartItems: [],
          currentShippingAddress: null,
          summaryPrice: null);
    }
  }
}

class CheckoutStartResult extends UseCaseResult {
  final List<PaymentMethodModel> paymentMethods;
  final List<ShippingAddressModel> shippingAddress;
  final List<CartItem> cartItems;
  final ShippingAddressModel? currentShippingAddress;
  final PaymentMethodModel? currentPaymentMethod;
  final double? totalCalculatedPrice;
  final double? deliveryPrice;
  final double? summaryPrice;

  CheckoutStartResult(
      {required this.paymentMethods,
      required this.shippingAddress,
      required this.cartItems,
      required this.currentShippingAddress,
      required this.currentPaymentMethod,
      required this.totalCalculatedPrice,
      required this.deliveryPrice,
      required this.summaryPrice,
      required Exception? exception,
      required bool result})
      : super(exception: exception, result: result);
}

class CheckoutStartParams {}

class CheckoutStartException implements Exception {}
