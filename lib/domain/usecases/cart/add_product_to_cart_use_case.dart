import 'package:ecommerce/data/model/cart_item.dart';
import 'package:ecommerce/data/repositories/abstract/cart_repository.dart';
import 'package:ecommerce/domain/usecases/base_use_case.dart';
import 'package:ecommerce/locator.dart';

abstract class AddProductToCartUseCase
    implements BaseUseCase<AddToCartResult, CartItem> {}

class AddProductToCartUseCaseImpl implements AddProductToCartUseCase {

  @override
  Future<AddToCartResult> execute(CartItem item) async {
    try {
      CartRepository _cartRepository = sl();
      _cartRepository.addProductToCart(item);
      return AddToCartResult(exception: null, result: true);
    } catch (e) {
      return AddToCartResult(
        exception: AddProductToCartException(),
        result: false,
      );
    }
  }
}

class AddProductToCartException implements Exception {}

class AddToCartResult extends UseCaseResult {
  AddToCartResult({required Exception? exception, required bool result})
      : super(exception: exception, result: result);
}
