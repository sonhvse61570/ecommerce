
import 'package:ecommerce/data/model/cart_item.dart';
import 'package:ecommerce/data/repositories/abstract/cart_repository.dart';
import 'package:ecommerce/domain/usecases/base_use_case.dart';
import 'package:ecommerce/locator.dart';

abstract class RemoveProductFromCartUseCase
    implements BaseUseCase<RemoveProductFromCartResult, CartItem> {}


class RemoveProductFromCartUseCaseImpl implements RemoveProductFromCartUseCase {
  @override
  Future<RemoveProductFromCartResult> execute(CartItem item) async {
    try {
      CartRepository cartRepository = sl();
      cartRepository.removeProductFromCart(item);
      return RemoveProductFromCartResult(result: true, exception: null);
    } catch (e) {
      return RemoveProductFromCartResult(
          result: false,
          exception: AddProductToCartException()
      );
    }
  }
}

class AddProductToCartException implements Exception {}

class RemoveProductFromCartResult extends UseCaseResult {
  RemoveProductFromCartResult({required Exception? exception, required bool result})
      : super(exception: exception, result: result);
}