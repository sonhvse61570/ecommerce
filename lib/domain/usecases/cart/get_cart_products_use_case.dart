import 'package:ecommerce/data/model/cart_item.dart';
import 'package:ecommerce/data/model/promo.dart';
import 'package:ecommerce/data/repositories/abstract/cart_repository.dart';
import 'package:ecommerce/domain/usecases/base_use_case.dart';
import 'package:ecommerce/locator.dart';

abstract class GetCartProductsUseCase
    implements BaseUseCase<GetCartProductsResult, GetCartProductParams> {}

class GetCartProductsUseCaseImpl implements GetCartProductsUseCase {
  @override
  Future<GetCartProductsResult> execute(GetCartProductParams params) async {
    try {
      CartRepository cartRepository = sl();
      if (params.appliedPromo != null) {
        await cartRepository.setPromo(params.appliedPromo);
      }
      List<CartItem> cartProducts = await cartRepository.getCartContent();

      return GetCartProductsResult(
        cartItems: cartProducts,
        totalPrice: cartRepository.getTotalPrice(),
        calculatedPrice: cartRepository.getCalculatedPrice(),
        appliedPromo: await cartRepository.getAppliedPromo(),
        result: true,
        exception: null,
      );
    } catch (e) {
      return GetCartProductsResult(
        cartItems: [],
        totalPrice: 0,
        calculatedPrice: 0,
        result: false,
        exception: GetCartProductsException(),
        appliedPromo: null,
      );
    }
  }
}

class GetCartProductParams {
  final Promo? appliedPromo;

  GetCartProductParams({this.appliedPromo});
}

class GetCartProductsException implements Exception {}

class GetCartProductsResult extends UseCaseResult {
  final List<CartItem> cartItems;
  final double totalPrice;
  final double calculatedPrice;
  final Promo? appliedPromo;

  GetCartProductsResult(
      {required this.cartItems,
      required this.totalPrice,
      required this.calculatedPrice,
      required this.appliedPromo,
      required Exception? exception,
      required bool result})
      : super(exception: exception, result: result);
}
