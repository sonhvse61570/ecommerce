import 'package:ecommerce/data/model/cart_item.dart';
import 'package:ecommerce/data/repositories/abstract/cart_repository.dart';
import 'package:ecommerce/domain/usecases/base_use_case.dart';
import 'package:ecommerce/locator.dart';

abstract class ChangeCartItemQuantityUseCase
    implements
        BaseUseCase<ChangeCartItemQuantityResult,
            ChangeCartItemQuantityParams> {}

class ChangeCartItemQuantityUseCaseImpl
    implements ChangeCartItemQuantityUseCase {

  @override
  Future<ChangeCartItemQuantityResult> execute(
      ChangeCartItemQuantityParams params) async {
    try {
      CartRepository _cartRepository = sl();
      await _cartRepository.changeQuantity(params.item, params.quantity);
      return ChangeCartItemQuantityResult(exception: null, result: true);
    } catch (e) {
      return ChangeCartItemQuantityResult(
          exception: ChangeCartItemQuantityException(), result: false);
    }
  }
}

class ChangeCartItemQuantityParams {
  final CartItem item;
  final int quantity;

  ChangeCartItemQuantityParams({required this.item, required this.quantity});
}

class ChangeCartItemQuantityResult extends UseCaseResult {
  ChangeCartItemQuantityResult(
      {required Exception? exception, required bool result})
      : super(exception: exception, result: result);
}

class ChangeCartItemQuantityException implements Exception {}
