import 'package:ecommerce/data/model/promo.dart';
import 'package:ecommerce/data/repositories/abstract/promo_repository.dart';
import 'package:ecommerce/domain/usecases/base_use_case.dart';
import 'package:ecommerce/locator.dart';

abstract class GetPromosUseCase
    implements BaseUseCase<GetPromosResult, GetPromosParams> {}

class GetPromosUseCaseImpl implements GetPromosUseCase {
  @override
  Future<GetPromosResult> execute(GetPromosParams params) async {
    try {
      PromoRepository promoRepository = sl();
      var promos = await promoRepository.getPromoList();

      if (promos.isNotEmpty) {
        return GetPromosResult(
          promos: promos,
          result: true,
          exception: null,
        );
      }
    } catch (e) {
      return GetPromosResult(
          promos: [], result: false, exception: EmptyProductsException());
    }
    return GetPromosResult(
        promos: [], result: false, exception: EmptyProductsException());
  }
}

class GetPromosResult extends UseCaseResult {
  List<Promo> promos;

  GetPromosResult(
      {required this.promos,
      required Exception? exception,
      required bool result})
      : super(exception: exception, result: result);
}

class GetPromosParams {}

class EmptyProductsException implements Exception {}
