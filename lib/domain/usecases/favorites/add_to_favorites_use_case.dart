import 'package:ecommerce/data/model/favorite_product.dart';
import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/repositories/abstract/favorites_repository.dart';
import 'package:ecommerce/data/repositories/abstract/product_repository.dart';
import 'package:ecommerce/domain/usecases/base_use_case.dart';
import 'package:ecommerce/locator.dart';

abstract class AddToFavoritesUseCase
    implements BaseUseCase<AddToFavoriteResult, FavoriteProduct> {}


class AddToFavoritesUseCaseImpl implements AddToFavoritesUseCase {
  @override
  Future<AddToFavoriteResult> execute(FavoriteProduct item) async {
    try {
      FavoritesRepository _productRepository = sl();
      _productRepository.addToFavorites(item);

      return AddToFavoriteResult(result: true, exception: null);
    } catch (e) {
      return AddToFavoriteResult(
          result: false,
          exception: AddProductToCartException()
      );
    }
  }

}

class AddProductToCartException implements Exception {}

class AddToFavoriteResult extends UseCaseResult {
  AddToFavoriteResult({required Exception? exception, required bool result})
      : super(exception: exception, result: result);
}