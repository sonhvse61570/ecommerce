import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/favorite_product.dart';
import 'package:ecommerce/data/model/filter_rules.dart';
import 'package:ecommerce/data/model/sort_rules.dart';
import 'package:ecommerce/data/repositories/abstract/favorites_repository.dart';
import 'package:ecommerce/domain/usecases/base_use_case.dart';
import 'package:ecommerce/locator.dart';

abstract class GetFavoriteProductsUseCase
    implements
        BaseUseCase<GetFavoriteProductResult, GetFavoriteProductParams> {}

class GetFavoriteProductsUseCaseImpl implements GetFavoriteProductsUseCase {
  @override
  Future<GetFavoriteProductResult> execute(
      GetFavoriteProductParams params) async {
    try {
      FavoritesRepository favoriteRepository = sl();
      var favoriteProducts = await favoriteRepository.getFavoriteProducts(
        pageIndex: params.pageIndex,
        pageSize: params.pageSize,
        sortRules: params.sortRules,
        filterRules: params.filterRules!,
      );

      return GetFavoriteProductResult(
        products: favoriteProducts,
        filterRules:
            FilterRules.getFavoriteSelectableAttributes(favoriteProducts),
        result: true,
        exception: null,
      );
    } catch (e) {
      return GetFavoriteProductResult(
        result: false,
        exception: GetFavoriteProductException(),
        products: [],
        filterRules: null,
      );
    }
  }
}

class GetFavoriteProductParams {
  final int pageIndex;
  final int pageSize;
  final SortRules sortRules;
  final FilterRules? filterRules;

  GetFavoriteProductParams({
    this.pageIndex = 0,
    this.pageSize = AppConsts.pageSize,
    this.sortRules = const SortRules(),
    this.filterRules,
  });
}

class GetFavoriteProductResult extends UseCaseResult {
  final List<FavoriteProduct> products;
  final FilterRules? filterRules;

  GetFavoriteProductResult({
    required this.products,
    required this.filterRules,
    required Exception? exception,
    required bool result,
  }) : super(exception: exception, result: result);
}

class GetFavoriteProductException implements Exception {}
