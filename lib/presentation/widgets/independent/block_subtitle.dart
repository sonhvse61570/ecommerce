
import 'package:ecommerce/config/app_theme.dart';
import 'package:flutter/material.dart';

class OpenFlutterBlockSubtitle extends StatelessWidget {
  final double? width;
  final String title;
  final String? linkText;
  final Function? onLinkTap;

  const OpenFlutterBlockSubtitle({
    Key? key,
    this.width,
    required this.title,
    this.linkText,
    this.onLinkTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);
    const rightLinkWidth = 100.0;
    return Container(
      padding: const EdgeInsets.only(
          top: AppSizes.sidePadding, left: AppSizes.sidePadding),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(title,
                    style: _theme.textTheme.headline4?.copyWith(
                        fontWeight: FontWeight.bold,
                        color: _theme.primaryColor)),
                linkText != null
                    ? InkWell(
                        onTap: (() => {onLinkTap!()}),
                        child: SizedBox(
                          width: rightLinkWidth,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              linkText!,
                              style: _theme.textTheme.headline4
                                  ?.copyWith(color: _theme.colorScheme.secondary),
                            ),
                          ),
                        ),
                      )
                    : Container()
              ],
            ),
          ],
        ),
      ),
    );
  }
}
