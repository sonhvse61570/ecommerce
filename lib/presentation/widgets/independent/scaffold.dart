import 'package:flutter/material.dart';

import '../widgets.dart';

class OpenFlutterScaffold extends StatelessWidget {
  final Color? background;
  final String? title;
  final Widget body;
  final int bottomMenuIndex;
  final List<String>? tabBarList;
  final TabController? tabController;

  const OpenFlutterScaffold(
      {Key? key,
      this.background,
      required this.title,
      required this.body,
      required this.bottomMenuIndex,
      this.tabBarList,
      this.tabController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var tabBars = <Tab>[];
    var _theme = Theme.of(context);
    if (tabBarList != null) {
      for (var i = 0; i < tabBarList!.length; i++) {
        tabBars.add(Tab(key: UniqueKey(), text: tabBarList![i]));
      }
    }
    PreferredSizeWidget tabWidget = tabBars.isNotEmpty == true
        ? TabBar(
            unselectedLabelColor: _theme.primaryColor,
            unselectedLabelStyle:
                const TextStyle(fontWeight: FontWeight.normal),
            labelColor: _theme.primaryColor,
            labelStyle: const TextStyle(fontWeight: FontWeight.bold),
            tabs: tabBars,
            controller: tabController,
            indicatorColor: _theme.colorScheme.secondary,
            indicatorSize: TabBarIndicatorSize.tab)
        : const TabBar(
            tabs: [],
          );
    return DefaultTabController(
      length: tabBars.length,
      child: Scaffold(
        backgroundColor: background,
        appBar: title != null
            ? AppBar(title: Text(title!), bottom: tabWidget, actions: <Widget>[
                Row(children: const <Widget>[
                  Icon(Icons.share),
                ])
              ])
            : null,
        body: body,
        bottomNavigationBar: OpenFlutterBottomMenu(
          menuIndex: bottomMenuIndex,
        ),
      ),
    );
  }
}
