import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/widgets/independent/bottom_menu.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class OpenFlutterCollapsingScaffold extends StatelessWidget {
  final Color? background;
  final String? title;
  final Widget body;
  final int bottomMenuIndex;
  final List<String>? tabBarList;
  final TabController tabController;

  const OpenFlutterCollapsingScaffold(
      {Key? key,
      this.background,
      required this.title,
      required this.body,
      required this.bottomMenuIndex,
      required this.tabBarList,
      required this.tabController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: NestedScrollView(
        physics: const ScrollPhysics(parent: PageScrollPhysics()),
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return _buildSilverAppBar(context);
        },
        body: body,
      ),
      backgroundColor: background,
      bottomNavigationBar: OpenFlutterBottomMenu(menuIndex: bottomMenuIndex),
    );
  }

  List<Widget> _buildSilverAppBar(BuildContext context) {
    var tabBars = <Tab>[];
    var _theme = Theme.of(context);
    if (tabBarList != null) {
      for (var i = 0; i < (tabBarList?.length ?? 0); i++) {
        tabBars.add(Tab(key: UniqueKey(), text: tabBarList![i]));
      }
    }

    PreferredSizeWidget tabWidget = tabBars.isNotEmpty
        ? TabBar(
            unselectedLabelColor: _theme.primaryColor,
            unselectedLabelStyle:
                const TextStyle(fontWeight: FontWeight.normal),
            labelColor: _theme.primaryColor,
            labelStyle: const TextStyle(fontWeight: FontWeight.bold),
            tabs: tabBars,
            controller: tabController,
            indicatorColor: _theme.colorScheme.secondary,
            indicatorSize: TabBarIndicatorSize.tab)
        : const TabBar(
            tabs: [],
          );

    return <Widget>[
      SliverAppBar(
          expandedHeight: AppSizes.appBarExpandedSize,
          floating: false,
          pinned: true,
          bottom: tabWidget,
          actions: <Widget>[
            Row(children: <Widget>[
              IconButton(
                icon: const Icon(Icons.search),
                color: AppColors.black,
                onPressed: () {
                  if (kDebugMode) {
                    print('Search favourites.');
                  }
                },
              ),
            ])
          ],
          flexibleSpace: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              var percent = ((constraints.maxHeight - kToolbarHeight) *
                  100.0 /
                  (AppSizes.appBarExpandedSize - kToolbarHeight));
              var dx = 0.0;

              dx = 100.0 - percent;

              if (constraints.maxHeight == 100) {
                dx = 0;
              }

              return Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: kToolbarHeight / 4, left: 0.0),
                    child: Transform.translate(
                      child: Text(
                        title ?? "",
                        style: _theme.textTheme.caption,
                      ),
                      offset:
                          Offset(dx, constraints.maxHeight - kToolbarHeight),
                    ),
                  ),
                ],
              );
            },
          )),
    ];
  }
}
