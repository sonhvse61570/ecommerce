import 'package:ecommerce/config/app_theme.dart';
import 'package:flutter/material.dart';

class OpenFlutterFavouriteButton extends StatelessWidget {
  final bool? favourite;
  final VoidCallback? setFavourite;
  final double? size;
  final double? iconSize;

  const OpenFlutterFavouriteButton(
      {Key? key, this.size, this.iconSize, this.favourite, this.setFavourite}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);
    return InkWell(
      onTap: setFavourite,
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.white,
          boxShadow: [
            BoxShadow(color: _theme.primaryColorLight, blurRadius: 15),
          ],
          borderRadius: const BorderRadius.all(Radius.circular(25)),
        ),
        height: size ?? 50,
        width: size ?? 50,
        child: favourite!
            ? Icon(Icons.favorite,
                size: iconSize ?? 20, color: _theme.colorScheme.secondary)
            : Icon(Icons.favorite_border,
                size: iconSize ?? 20, color: _theme.primaryColorLight),
      ),
    );
  }
}
