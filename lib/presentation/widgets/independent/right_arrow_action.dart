import 'package:ecommerce/config/app_theme.dart';
import 'package:flutter/material.dart';

class OpenFlutterRightArrow extends StatelessWidget {
  final String text;
  final VoidCallback onClick;

  const OpenFlutterRightArrow(this.text, {Key? key, required this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: GestureDetector(
        onTap: onClick,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(text),
            const Icon(
              Icons.trending_flat,
              color: AppColors.red,
            )
          ],
        ),
      ),
    );
  }
}
