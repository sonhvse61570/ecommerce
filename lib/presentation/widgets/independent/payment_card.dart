import 'package:ecommerce/config/app_theme.dart';
import 'package:flutter/material.dart';

class OpenFlutterPaymentCard extends StatelessWidget {
  final String cardNumber;

  const OpenFlutterPaymentCard({Key? key, required this.cardNumber}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);
    return Container(
        padding: const EdgeInsets.all(AppSizes.sidePadding),
        child: Row(
          children: <Widget>[
            Container(
              width: 64,
              height: 38,
              color: AppColors.white,
              child: Image.asset('assets/images/checkout/mastercard.png', height: 38),
            ),
            Container(
              padding: const EdgeInsets.only(left: AppSizes.sidePadding),
              child: Text(cardNumber, style: _theme.textTheme.headline2?.copyWith(color: _theme.primaryColor)),
            )
          ],
        ));
  }
}
