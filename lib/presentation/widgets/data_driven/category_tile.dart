import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/category.dart';
import 'package:ecommerce/presentation/widgets/extensions/commerce_image_view.dart';
import 'package:flutter/material.dart';

class OpenFlutterCategoryTile extends StatelessWidget {
  final ProductCategory category;
  final double height;
  final double width;

  const OpenFlutterCategoryTile({
    Key? key,
    required this.category,
    required this.height,
    required this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);
    return Padding(
        padding: const EdgeInsets.only(bottom: AppSizes.sidePadding),
        child: Container(
            padding: const EdgeInsets.only(left: AppSizes.sidePadding),
            height: height,
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.circular(AppSizes.imageRadius),
            ),
            child: Row(
              children: <Widget>[
                Container(
                    alignment: Alignment.centerLeft,
                    width: width - 200.0,
                    child:
                        Text(category.name, style: _theme.textTheme.headline4)),
                Container(
                  width: 200,
                  alignment: Alignment.centerRight,
                  child: Image(image: category.image.getView()),
                )
              ],
            )));
  }
}
