import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/hashtag.dart';
import 'package:flutter/material.dart';

class OpenFlutterHashTagList extends StatelessWidget {
  final List<HashTag> tags;
  final double height;
  final Function onTap;

  const OpenFlutterHashTagList(
      {Key? key, required this.tags, required this.onTap, required this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var hashtagButtons = <Widget>[];
    var _theme = Theme.of(context);
    for (var i = 0; i < tags.length; i++) {
      hashtagButtons.add(buildButton(tags[i], _theme));
    }
    return SizedBox(
      height: height,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: hashtagButtons,
      ),
    );
  }

  Padding buildButton(HashTag tag, ThemeData _theme) {
    return Padding(
        padding: const EdgeInsets.only(right: AppSizes.sidePadding / 2),
        child: Container(
          padding: const EdgeInsets.all(
            AppSizes.linePadding,
          ),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: _theme.primaryColor,
            borderRadius: BorderRadius.circular(
              AppSizes.buttonRadius,
            ),
          ),
          child: Text(
            tag.title,
            style: _theme.textTheme.button,
          ),
          width: 100,
          height: 20,
        ));
  }
}
