import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/widgets/independent/base_product_list_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BlankProductListItem extends BaseProductListItem {
  BlankProductListItem()
      : super(
          inactiveMessage: '',
          bottomRoundButton: Container(
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: AppColors.background,
            ),
            height: 36,
            width: 36,
          ),
          mainContentBuilder: (context) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _buildGrey(115, 21),
                _buildGrey(38, 11),
                _buildGrey(91, 14),
                _buildGrey(23, 15),
              ],
            );
          },
        );

  static Widget _buildGrey(double width, double height) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        width: width,
        height: height,
        decoration: const BoxDecoration(
            color: AppColors.background,
            borderRadius: BorderRadius.all(Radius.circular(4.0))),
      ),
    );
  }
}
