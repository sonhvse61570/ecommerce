// Category element in list view
// Author: openflutterproject@gmail.com
// Date: 2020-02-06

import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/category.dart';
import 'package:flutter/material.dart';

class OpenFlutterCategoryListElement extends StatelessWidget {
  final ProductCategory category;

  const OpenFlutterCategoryListElement({Key? key, required this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(
            top: AppSizes.sidePadding,
            bottom: AppSizes.sidePadding,
            left: AppSizes.sidePadding * 2,
            right: AppSizes.sidePadding),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: _theme.primaryColorLight, width: 0.4),
          ),
        ),
        child: Text(category.name,
            style: _theme.textTheme.headline4
                ?.copyWith(fontWeight: FontWeight.normal)));
  }
}
