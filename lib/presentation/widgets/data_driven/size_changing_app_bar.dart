import 'dart:collection';

import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/filter_rules.dart';
import 'package:ecommerce/data/model/hashtag.dart';
import 'package:ecommerce/data/model/sort_rules.dart';
import 'package:ecommerce/presentation/features/products/products_bloc.dart';
import 'package:ecommerce/presentation/features/products/products_event.dart';
import 'package:ecommerce/presentation/features/products/views/visual_filter.dart';
import 'package:ecommerce/presentation/widgets/independent/view_options.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SizeChangingAppBar extends StatelessWidget {
  final String? title;
  final FilterRules? filterRules;
  final SortRules? sortRules;
  final bool isListView;
  final Function(FilterRules) onFilterRulesChanged;
  final Function(SortRules) onSortRulesChanged;
  final VoidCallback onViewChanged;

  const SizeChangingAppBar(
      {Key? key,
      required this.title,
      this.filterRules,
      this.sortRules,
      this.isListView = true,
      required this.onFilterRulesChanged,
      required this.onSortRulesChanged,
      required this.onViewChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      expandedHeight: AppSizes.appBarExpandedSize,
      actions: const <Widget>[
        IconButton(
          icon: Icon(Icons.search),
          onPressed: null, //TODO add search
        )
      ],
      floating: true,
      primary: true,
      snap: false,
      pinned: false,
      /*bottom: PreferredSize(
        preferredSize: Size.fromHeight(70),
        child: ,
      ),*/
      flexibleSpace: FlexibleSpaceBar(
        background: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Text(
                title ?? 'Loading...',
                style: Theme.of(context).textTheme.caption,
              ),
            ),
            SizedBox(
              height: 30,
              child: VisualFilter(filterRules?.hashTags ?? [],
                  filterRules?.selectedHashTags ?? HashMap<HashTag, bool>(),
                  (updateValue, isSelected) {
                BlocProvider.of<ProductsBloc>(context)
                    .add(ProductChangeHashTagEvent(updateValue, isSelected));
              }),
            ),
            OpenFlutterViewOptions(
              sortRules: sortRules,
              filterRules: filterRules,
              isListView: isListView,
              onChangeViewClicked: onViewChanged,
              onFilterChanged: onFilterRulesChanged,
              onSortChanged: onSortRulesChanged,
            ),
          ],
        ),
      ),
    );
  }
}
