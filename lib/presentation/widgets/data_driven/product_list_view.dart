
import 'package:ecommerce/config/app_routes.dart';
import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/presentation/features/product_details/product_details_screen.dart';
import 'package:ecommerce/presentation/widgets/extensions/product_view.dart';
import 'package:flutter/material.dart';

class OpenFlutterProductListView extends StatelessWidget {
  final double width;
  final double height = 300;
  final List<Product> products;
  final Function(Product product) onFavoritesTap;

  const OpenFlutterProductListView({
    required this.width,
    required this.products,
    required this.onFavoritesTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: AppSizes.sidePadding),
      width: width,
      height: height,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: products
          .map((product) => product.getTileView(
            context: context,
            onFavoritesClick: ( () => {
              onFavoritesTap(product)
            }), 
            showProductInfo: () {
              Navigator.of(context).pushNamed(
                  AppRoutes.product,
                  arguments: ProductDetailsParameters(product.id,
                    product.categories.isNotEmpty ?
                      product.categories[0].id : 0
                    )
                  );
            },
          )
        ).toList(growable: false))
      );
  }
}
