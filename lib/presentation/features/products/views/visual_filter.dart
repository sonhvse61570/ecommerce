import 'dart:collection';

import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/hashtag.dart';
import 'package:flutter/material.dart';

class VisualFilter extends StatelessWidget {
  final List<HashTag>? hashTags;
  final HashMap<HashTag, bool>? selectedHashTags;
  final FilterChanged onFilterChanged;

  const VisualFilter(this.hashTags, this.selectedHashTags, this.onFilterChanged,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (hashTags == null) {
      return ListView.builder(
          scrollDirection: Axis.horizontal, itemBuilder: _blankChip);
    } else {
      List<Widget> widgetList = hashTags
          ?.map((optionHashTag) => Padding(
                padding: const EdgeInsets.only(right: AppSizes.sidePadding / 2),
                child: ChoiceChip(
                  selected: selectedHashTags != null
                      ? selectedHashTags![optionHashTag] ?? false
                      : false,
                  padding: const EdgeInsets.all(
                    AppSizes.linePadding,
                  ),
                  backgroundColor: Theme.of(context).primaryColor,
                  selectedColor: Theme.of(context).colorScheme.secondary,
                  label: Text(
                    optionHashTag.title,
                    style: Theme.of(context).textTheme.button,
                  ),
                  onSelected: (value) {
                    onFilterChanged(optionHashTag, value);
                  },
                ),
              ))
          .toList(growable: false) ?? [];

      return ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
              const SizedBox(
                width: 16,
              )
            ] +
            widgetList +
            [
              const SizedBox(
                width: 16,
              )
            ],
      );
    }
  }

  Widget _blankChip(BuildContext context, _) {
    return Padding(
        padding: const EdgeInsets.only(right: AppSizes.sidePadding / 2),
        child: Chip(
          padding: const EdgeInsets.all(
            AppSizes.linePadding,
          ),
          backgroundColor: Theme.of(context).backgroundColor,
          label: Text(
            '        ',
            style: Theme.of(context).textTheme.button,
          ),
        ));
  }
}

typedef FilterChanged = Function(HashTag attribute, bool isSelected);
