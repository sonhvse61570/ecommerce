// List of products in the card view
// Author: openflutterproject@gmail.com
// Date: 2020-02-06

import 'package:ecommerce/config/app_routes.dart';
import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/widgets/extensions/product_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../products.dart';

class ProductsTileView extends StatelessWidget {
  const ProductsTileView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductsBloc, ProductsState>(builder: (context, state) {
      return SliverGrid(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 4.0,
          crossAxisSpacing: 4.0,
          childAspectRatio: AppSizes.tileWidth / AppSizes.tileHeight,
        ),
        delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return Padding(
                padding: const EdgeInsets.symmetric(horizontal: AppSizes.sidePadding),
                child: state.data?.products[index].getTileView(
                  context: context,
                  onFavoritesClick: () {
                    BlocProvider.of<ProductsBloc>(context).add(
                        ProductMakeFavoriteEvent(
                            !state.data!.products[index].isFavorite,
                            state.data!.products[index]));
                  },
                  showProductInfo: () {
                    Navigator.of(context).pushNamed(
                        AppRoutes.product,
                        arguments: ProductDetailsParameters(
                            state.data!.products[index].id,
                            state.data!.category.id));
                  },
                ));
          },
          childCount: state.data!.products.length,
        ),
      );
    });
  }
}
