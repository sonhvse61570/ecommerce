import 'package:ecommerce/data/model/product.dart';
import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class HomeLoadEvent extends HomeEvent {
  @override
  String toString() => 'Home is Loaded';
}

class HomeAddToFavoriteEvent extends HomeEvent {
  final bool? isFavorite;
  final Product? product;

  HomeAddToFavoriteEvent(this.isFavorite, this.product);

  @override
  List<Object?> get props => [isFavorite, product];
}