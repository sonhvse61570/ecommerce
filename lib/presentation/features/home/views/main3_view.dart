import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_button.dart';
import 'package:flutter/material.dart';

class Main3View extends StatefulWidget {
  final Function? changeView;

  const Main3View({Key? key, this.changeView}) : super(key: key);

  @override
  _Main3ViewState createState() => _Main3ViewState();
}

class _Main3ViewState extends State<Main3View> {
  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);
    var width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
        child: Column(children: <Widget>[
      Container(
          height: width * 0.96,
          width: width,
          decoration: const BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage('assets/splash/main3.png'),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  alignment: Alignment.centerRight,
                  padding: const EdgeInsets.only(
                      left: AppSizes.sidePadding, bottom: AppSizes.sidePadding),
                  width: width,
                  child: Text('New collection',
                      style:
                          _theme.textTheme.headline5?.copyWith(fontSize: 34)))
            ],
          )),
      Row(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  alignment: Alignment.centerLeft,
                  width: width / 2,
                  height: width / 2 - 2,
                  padding: const EdgeInsets.all(AppSizes.sidePadding),
                  child: Text('Summer sale',
                      style: _theme.textTheme.headline5?.copyWith(
                          fontSize: 34, color: _theme.colorScheme.secondary))),
              Container(
                  height: width / 2,
                  width: width / 2,
                  alignment: Alignment.bottomLeft,
                  padding: const EdgeInsets.only(
                      bottom: AppSizes.sidePadding, left: AppSizes.sidePadding),
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage('assets/splash/bottombanner.png'),
                    ),
                  ),
                  child: Text('Black',
                      style:
                          _theme.textTheme.headline5?.copyWith(fontSize: 34))),
            ],
          ),
          Container(
              height: width / 2 * 1.99,
              width: width / 2,
              alignment: Alignment.center,
              padding: const EdgeInsets.only(left: AppSizes.sidePadding),
              decoration: const BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage('assets/splash/sidebanner.png'),
                ),
              ),
              child: Text('Men’s hoodies',
                  style: _theme.textTheme.headline5?.copyWith(fontSize: 34))),
        ],
      ),
      const Padding(padding: EdgeInsets.only(top: AppSizes.sidePadding)),
      OpenFlutterButton(
          title: 'Next Home Page',
          width: 160,
          height: 48,
          onPressed: (() =>
              widget.changeView!(changeType: ViewChangeType.Start)))
    ]));
  }
}
