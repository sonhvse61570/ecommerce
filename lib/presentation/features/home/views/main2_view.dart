import 'package:ecommerce/config/app_routes.dart';
import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/presentation/features/categories/categories_screen.dart';
import 'package:ecommerce/presentation/features/home/home_bloc.dart';
import 'package:ecommerce/presentation/features/home/home_event.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/data_driven/product_list_view.dart';
import 'package:ecommerce/presentation/widgets/independent/block_header.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Main2View extends StatefulWidget {
  final Function? changeView;
  final List<Product>? salesProducts;
  final List<Product>? newProducts;

  const Main2View(
      {Key? key, this.changeView, this.salesProducts, this.newProducts})
      : super(key: key);

  @override
  _Main2ViewState createState() => _Main2ViewState();
}

class _Main2ViewState extends State<Main2View> {
  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);
    var width = MediaQuery.of(context).size.width;
    var widgetWidth = width - AppSizes.sidePadding * 2;
    return SingleChildScrollView(
        child: Column(children: <Widget>[
      Container(
          height: width * 0.52,
          width: width,
          decoration: const BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage('assets/splash/topbanner.png'),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  padding: const EdgeInsets.only(left: AppSizes.sidePadding),
                  width: width,
                  child: Text('Street clothes',
                      style:
                          _theme.textTheme.headline5?.copyWith(fontSize: 34)))
            ],
          )),
      OpenFlutterBlockHeader(
        width: widgetWidth,
        title: 'Sale',
        linkText: 'View All',
        onLinkTap: () => {
          Navigator.of(context).pushNamed(AppRoutes.shop,
              arguments: const CategoriesParameters(2))
        },
        description: 'Super summer sale',
      ),
      OpenFlutterProductListView(
          width: widgetWidth,
          products: widget.salesProducts!,
          onFavoritesTap: ((Product product) => {
                BlocProvider.of<HomeBloc>(context)
                    .add(HomeAddToFavoriteEvent(!product.isFavorite, product))
              })),
      const Padding(padding: EdgeInsets.only(top: AppSizes.sidePadding)),
      OpenFlutterBlockHeader(
        width: widgetWidth,
        title: 'New',
        linkText: 'View All',
        onLinkTap: () => {
          Navigator.of(context).pushNamed(AppRoutes.shop,
              arguments: const CategoriesParameters(3))
        },
        description: 'You’ve never seen it before!',
      ),
      OpenFlutterProductListView(
        width: widgetWidth,
        products: widget.newProducts!,
        onFavoritesTap: ((Product product) => {
              BlocProvider.of<HomeBloc>(context)
                  .add(HomeAddToFavoriteEvent(!product.isFavorite, product))
            }),
      ),
      OpenFlutterButton(
        title: 'Next Home Page',
        width: 160,
        height: 48,
        onPressed: (() =>
            widget.changeView!(changeType: ViewChangeType.Forward)),
      )
    ]));
  }
}
