import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'home_bloc.dart';
import 'home_event.dart';
import 'home_state.dart';
import 'views/main1_view.dart';
import 'views/main2_view.dart';
import 'views/main3_view.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: OpenFlutterScaffold(
          background: null,
          title: null,
          body: BlocProvider<HomeBloc>(
              create: (context) {
                return HomeBloc()..add(HomeLoadEvent());
              },
              child: const HomeWrapper()),
          bottomMenuIndex: 0,
        ));
  }
}


class HomeWrapper extends StatefulWidget {
  const HomeWrapper({Key? key}) : super(key: key);

  @override
  _HomeWrapperState createState() => _HomeWrapperState();
}

class _HomeWrapperState extends OpenFlutterWrapperState<HomeWrapper> {
  //State createState() => OpenFlutterWrapperState();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
        builder: (BuildContext context, HomeState state) {
          return getPageView(<Widget>[
            Main1View(
              changeView: changePage,
              products: state is HomeLoadedState ? state.newProducts : <Product>[],
            ),
            Main2View(
                changeView: changePage,
                salesProducts:
                state is HomeLoadedState ? state.salesProducts : <Product>[],
                newProducts:
                state is HomeLoadedState ? state.newProducts : <Product>[]),
            Main3View(changeView: changePage)
          ]);
        });
  }
}
