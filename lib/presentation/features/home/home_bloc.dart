import 'package:bloc/bloc.dart';
import 'package:ecommerce/data/model/favorite_product.dart';
import 'package:ecommerce/domain/usecases/favorites/add_to_favorites_use_case.dart';
import 'package:ecommerce/domain/usecases/favorites/remove_from_favorites_use_case.dart';
import 'package:ecommerce/domain/usecases/products/get_home_products_use_case.dart';
import 'package:ecommerce/locator.dart';

import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final AddToFavoritesUseCase addToFavoritesUseCase;
  final RemoveFromFavoritesUseCase removeFromFavoritesUseCase;
  final GetHomePageProductsUseCase getHomePageProductsUseCase;

  HomeBloc()
      : addToFavoritesUseCase = sl(),
        removeFromFavoritesUseCase = sl(),
        getHomePageProductsUseCase = sl(),
        super(HomeInitialState()) {
    on<HomeLoadEvent>(_onLoaded);
    on<HomeAddToFavoriteEvent>(_onFavoriteAdded);
  }

  Future<void> _onLoaded(
    HomeLoadEvent event,
    Emitter<HomeState> emit,
  ) async {
    if (state is HomeInitialState) {
      HomeProductsResult results =
          await getHomePageProductsUseCase.execute(HomeProductParams());
      emit(HomeLoadedState(
        results.salesProducts,
        results.newProducts,
      ));
    } else {
      emit(state);
    }
  }

  Future<void> _onFavoriteAdded(
    HomeAddToFavoriteEvent event,
    Emitter<HomeState> emit,
  ) async {
    if (event.isFavorite ?? false) {
      await addToFavoritesUseCase
          .execute(FavoriteProduct(event.product!, {}));
    } else {
      await removeFromFavoritesUseCase.execute(
          RemoveFromFavoritesParams(FavoriteProduct(event.product!, {})));
    }
  }
}
