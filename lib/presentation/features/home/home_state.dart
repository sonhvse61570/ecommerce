import 'package:ecommerce/data/model/product.dart';
import 'package:equatable/equatable.dart';

abstract class HomeState extends Equatable {
  @override
  List<Object?> get props => [];
}

class HomeInitialState extends HomeState {
  @override
  String toString() => 'HomeInitialState';
}

class HomeLoadedState extends HomeState {
  final List<Product>? salesProducts;
  final List<Product>? newProducts;

  HomeLoadedState(this.salesProducts, this.newProducts);

  @override
  String toString() => 'HomeLoadedState';

  @override
  List<Object?> get props => [salesProducts, newProducts];
}