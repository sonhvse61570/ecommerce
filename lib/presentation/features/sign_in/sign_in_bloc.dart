import 'package:ecommerce/data/repositories/abstract/user_repository.dart';
import 'package:ecommerce/presentation/features/authentication/authentication_bloc.dart';
import 'package:ecommerce/presentation/features/authentication/authentication_event.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'sign_in_event.dart';
import 'sign_in_state.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  SignInBloc({
    required this.userRepository,
    required this.authenticationBloc,
  }) : super(SignInInitialState()) {
    on<SignInPressed>(_onSignInPressed);
    on<SignInPressedFacebook>(_onSignInFacebookPressed);
    on<SignInPressedGoogle>(_onSignInGooglePressed);
  }

  Future<void> _onSignInPressed(
    SignInPressed event,
    Emitter<SignInState> emit,
  ) async {
    emit(SignInProcessingState());

    try {
      var token = await userRepository.signIn(
        email: event.email,
        password: event.password,
      );
      authenticationBloc.add(LoggedIn(token));
      emit(SignInFinishedState());
    } catch (error) {
      emit(SignInErrorState(error.toString()));
    }
  }

  Future<void> _onSignInFacebookPressed(
      SignInPressedFacebook event,
      Emitter<SignInState> emit,
      ) async {
    emit(SignInProcessingState());

    try {
      await Future.delayed(
        const Duration(milliseconds: 300),
      ); //TODO use real auth service

      emit(SignInFinishedState());
    } catch (error) {
      emit(SignInErrorState(error.toString()));
    }
  }

  Future<void> _onSignInGooglePressed(
      SignInPressedGoogle event,
      Emitter<SignInState> emit,
      ) async {
    emit(SignInProcessingState());

    try {
      await Future.delayed(
        const Duration(milliseconds: 300),
      ); //TODO use real auth service

      emit(SignInFinishedState());
    } catch (error) {
      emit(SignInErrorState(error.toString()));
    }
  }
}
