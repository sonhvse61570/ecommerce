import 'package:ecommerce/config/app_routes.dart';
import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/domain/entities/validator.dart';
import 'package:ecommerce/presentation/features/sign_in/sign_in_bloc.dart';
import 'package:ecommerce/presentation/features/sign_in/sign_in_event.dart';
import 'package:ecommerce/presentation/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'sign_in_state.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final GlobalKey<OpenFlutterInputFieldState> emailKey = GlobalKey();
  final GlobalKey<OpenFlutterInputFieldState> passwordKey = GlobalKey();

  late double sizeBetween;

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    sizeBetween = height / 20;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.transparent,
        elevation: 0,
        iconTheme: const IconThemeData(color: AppColors.black),
      ),
      backgroundColor: AppColors.background,
      body: BlocConsumer<SignInBloc, SignInState>(
        listener: (context, state) {
          // on success delete navigator stack and push to home
          if (state is SignInFinishedState) {
            Navigator.of(context).pushNamedAndRemoveUntil(
              AppRoutes.home,
                  (Route<dynamic> route) => false,
            );
          }
          // on failure show a snack bar
          if (state is SignInErrorState) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text(state.error),
                backgroundColor: Colors.red,
                duration: const Duration(seconds: 3),
              ),
            );
          }
        },
        builder: (context, state) {
          // show loading screen while processing
          if (state is SignInProcessingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return SingleChildScrollView(
            child: SizedBox(
              height: height * 0.9,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  OpenFlutterBlockHeader(title: 'Sign in', width: width),
                  SizedBox(
                    height: sizeBetween,
                  ),
                  OpenFlutterInputField(
                    key: emailKey,
                    controller: emailController,
                    hint: 'Email',
                    validator: Validator.validateEmail,
                    keyboard: TextInputType.emailAddress,
                  ),
                  OpenFlutterInputField(
                    key: passwordKey,
                    controller: passwordController,
                    hint: 'Password',
                    validator: Validator.passwordCorrect,
                    keyboard: TextInputType.visiblePassword,
                    isPassword: true,
                  ),
                  OpenFlutterRightArrow(
                    'Forgot your password',
                    onClick: _showForgotPassword,
                  ),
                  OpenFlutterButton(
                      title: 'LOGIN', onPressed: _validateAndSend),
                  SizedBox(
                    height: sizeBetween * 2,
                  ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: AppSizes.linePadding),
                    child: Center(
                      child: Text('Or sign up with social account'),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * 0.2),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: const <Widget>[
                        // OpenFlutterServiceButton(
                        //   serviceType: ServiceType.Google,
                        //   onPressed: () {
                        //     BlocProvider.of<SignUpBloc>(context).add(
                        //       SignUpPressedGoogle(),
                        //     );
                        //   },
                        // ),
                        // OpenFlutterServiceButton(
                        //   serviceType: ServiceType.Facebook,
                        //   onPressed: () {
                        //     BlocProvider.of<SignUpBloc>(context).add(
                        //       SignUpPressedFacebook(),
                        //     );
                        //   },
                        // ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  void _showForgotPassword() {
    Navigator.of(context).pushNamed(AppRoutes.forgotPassword);
  }

  void _validateAndSend() {
    if (!(emailKey.currentState?.validate().isEmpty ?? true)) {
      return;
    }
    if (!(passwordKey.currentState?.validate().isEmpty ?? true)) {
      return;
    }
    BlocProvider.of<SignInBloc>(context).add(
      SignInPressed(
        email: emailController.text,
        password: passwordController.text,
      ),
    );
  }
}
