import 'package:bloc/bloc.dart';
import 'package:ecommerce/data/repositories/abstract/user_repository.dart';

import 'forget_password_event.dart';
import 'forget_password_state.dart';

class ForgetPasswordBloc
    extends Bloc<ForgetPasswordEvent, ForgetPasswordState> {
  final UserRepository _userRepository;

  ForgetPasswordBloc({required UserRepository userRepository})
      : _userRepository = userRepository,
        super(ForgetPasswordInitialState()) {
    on<ForgetPasswordPressed>(_onPasswordRecovered);
  }

  Future<void> _onPasswordRecovered(
    ForgetPasswordPressed event,
    Emitter<ForgetPasswordState> emit,
  ) async {
    try {
      await _userRepository.forgotPassword(email: event.email);
    } catch (e) {
      emit(ForgetPasswordErrorState(e.toString()));
    }
  }
}
