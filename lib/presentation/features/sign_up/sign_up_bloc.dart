import 'package:ecommerce/data/repositories/abstract/user_repository.dart';
import 'package:ecommerce/presentation/features/authentication/authentication_bloc.dart';
import 'package:ecommerce/presentation/features/authentication/authentication_event.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'sign_up_event.dart';
import 'sign_up_state.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  SignUpBloc({
    required this.userRepository,
    required this.authenticationBloc,
  }) : super(SignUpInitialState()) {
    on<SignUpPressed>(_onSignUpPressed);
    on<SignUpPressedFacebook>(_onSignUpFacebookPressed);
    on<SignUpPressedGoogle>(_onSignUpGooglePressed);
  }

  Future<void> _onSignUpPressed(
    SignUpPressed event,
    Emitter<SignUpState> emit,
  ) async {
    emit(SignUpProcessingState());
    try {
      final token = await userRepository.signUp(
        name: event.name,
        email: event.email,
        password: event.password,
      );
      authenticationBloc.add(LoggedIn(token));
      emit(SignUpFinishedState());
    } catch (error) {
      emit(SignUpErrorState(error.toString()));
    }
  }

  Future<void> _onSignUpFacebookPressed(
    SignUpPressedFacebook event,
    Emitter<SignUpState> emit,
  ) async {
    emit(SignUpProcessingState());
    try {
      await Future.delayed(
        const Duration(milliseconds: 300),
      ); //TODO use real auth service

      emit(SignUpFinishedState());
    } catch (error) {
      emit(SignUpErrorState(error.toString()));
    }
  }

  Future<void> _onSignUpGooglePressed(
    SignUpPressedGoogle event,
    Emitter<SignUpState> emit,
  ) async {
    emit(SignUpProcessingState());
    try {
      await Future.delayed(
        const Duration(milliseconds: 300),
      ); //TODO use real auth service

      emit(SignUpFinishedState());
    } catch (error) {
      emit(SignUpErrorState(error.toString()));
    }
  }
}
