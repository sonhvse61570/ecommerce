import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_button.dart';
import 'package:flutter/material.dart';

class AcceptBottomNavigation extends StatelessWidget {
  const AcceptBottomNavigation({Key? key, this.onApply}) : super(key: key);

  final VoidCallback? onApply;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: AppColors.white,
      child: Row(
        children: <Widget>[
          Expanded(
            child: OpenFlutterButton(
              title: 'Discard',
              onPressed: () {
                Navigator.of(context).pop();
              },
              backgroundColor: AppColors.white,
              borderColor: AppColors.black,
              textColor: AppColors.black,
            ),
          ),
          Expanded(
            child: OpenFlutterButton(
              title: 'Apply',
              onPressed: onApply!,
            ),
          )
        ],
      ),
    );
  }
}
