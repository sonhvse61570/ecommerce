import 'package:ecommerce/config/app_theme.dart';
import 'package:flutter/material.dart';

class FilterSelectableItem extends StatelessWidget {
  const FilterSelectableItem({Key? key, this.isSelected = false, required this.text}) : super(key: key);

  final bool isSelected;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: AppSizes.linePadding),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(4)),
              border:
              Border.all(color: isSelected ? Theme.of(context).colorScheme.secondary : Theme.of(context).primaryColorLight),
              color: isSelected ? Theme.of(context).colorScheme.secondary : AppColors.white),
          padding: const EdgeInsets.symmetric(vertical: AppSizes.sidePadding, horizontal: AppSizes.sidePadding),
          child: Text(
            text,
            style: Theme.of(context).textTheme.headline2?.copyWith(
              color: isSelected ? AppColors.white : Theme.of(context).primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ));
  }
}
