import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/widgets/independent/block_subtitle.dart';
import 'package:flutter/material.dart';

class FilterSelectableVisibleOption<T> extends StatelessWidget {
  const FilterSelectableVisibleOption(
      {Key? key, required this.children, required this.onSelected, required this.title})
      : super(key: key);

  final Map<T, Widget> children;
  final ValueChanged<T> onSelected;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        OpenFlutterBlockSubtitle(title: title),
        const SizedBox(
          height: AppSizes.sidePadding,
        ),
        Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.symmetric(
              vertical: AppSizes.sidePadding,
              horizontal: AppSizes.sidePadding * 2),
          color: AppColors.white,
          child: Wrap(
            spacing: AppSizes.sidePadding,
            alignment: WrapAlignment.start,
            children: children
                .map((key, widget) => MapEntry(
                key,
                InkWell(
                  child: widget,
                  onTap: () {
                    onSelected(key);
                  },
                )))
                .values
                .toList(growable: false),
          ),
        )
      ],
    );
  }
}
