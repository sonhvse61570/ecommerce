import 'package:equatable/equatable.dart';

abstract class SettingsEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class UpdateFullNameEvent extends SettingsEvent {
  final String? fullName;

  UpdateFullNameEvent({this.fullName});

  @override
  List<Object?> get props => [fullName];

  @override
  String toString() => 'Update FullName Event';
}

class UpdateDateOfBirthEvent extends SettingsEvent {
  final String? dateOfBirth;

  UpdateDateOfBirthEvent({this.dateOfBirth});

  @override
  List<Object?> get props => [dateOfBirth];

  @override
  String toString() => 'Update DataOfBirth Event';
}

class UpdateNotifySalesEvent extends SettingsEvent {
  final bool? notifySales;

  UpdateNotifySalesEvent({this.notifySales});

  @override
  String toString() => 'Update NotifySales Event';

  @override
  List<Object?> get props => [notifySales];
}

class UpdateNotifyArrivalsEvent extends SettingsEvent {
  final bool? notifyArrivals;

  UpdateNotifyArrivalsEvent({this.notifyArrivals});

  @override
  String toString() => 'Update NotifyArrivals Event';

  @override
  List<Object?> get props => [notifyArrivals];
}

class UpdateNotifyDeliveryEvent extends SettingsEvent {
  final bool? notifyDelivery;

  UpdateNotifyDeliveryEvent({this.notifyDelivery});

  @override
  String toString() => 'Update Delivery Event';

  @override
  List<Object?> get props => [notifyDelivery];
}