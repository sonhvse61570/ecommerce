import 'package:bloc/bloc.dart';
import 'package:ecommerce/data/repositories/mock/order_mock_repository.dart';

import 'profile_event.dart';
import 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc() : super(ProfileInitialState()) {
    on<ProfileMyOrdersEvent>(_onViewOrders);
    on<ProfileMyOrderDetailsEvent>(_onViewOrderDetails);
  }

  Future<void> _onViewOrders(
    ProfileMyOrdersEvent event,
    Emitter<ProfileState> emit,
  ) async {
    emit(ProfileMyOrdersProcessingState());

    var repo = OrderMockRepository();
    var orders = repo.getMyOrders();
    emit(ProfileMyOrdersState(orderData: orders));
  }

  Future<void> _onViewOrderDetails(
    ProfileMyOrderDetailsEvent event,
    Emitter<ProfileState> emit,
  ) async {
    emit(ProfileMyOrdersProcessingState());

    var repo = OrderMockRepository();
    var order = repo.getOrderDetails(event.orderId);
    emit(ProfileMyOrderDetailsState(orderData: order));
  }
}
