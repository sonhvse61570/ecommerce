import 'package:ecommerce/data/model/user_order.dart';
import 'package:equatable/equatable.dart';

class ProfileState extends Equatable {
  @override
  List<Object?> get props => [];
}

class ProfileInitialState extends ProfileState {}

class ProfileMyOrdersProcessingState extends ProfileState {}

class ProfileMyOrdersState extends ProfileState {
  final List<UserOrder>? orderData;

  ProfileMyOrdersState({this.orderData});
}

class ProfileMyOrderDetailsState extends ProfileState {
  final UserOrder? orderData;

  ProfileMyOrderDetailsState({this.orderData});
}