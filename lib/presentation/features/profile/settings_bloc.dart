import 'package:bloc/bloc.dart';
import 'package:ecommerce/data/repositories/mock/settings_mock_repository.dart';
import 'package:ecommerce/domain/entities/user/settings.dart';

import 'settings_event.dart';
import 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  final SettingsMockRepository _settingsRepository;

  SettingsBloc({required SettingsMockRepository settingsRepository})
      : _settingsRepository = settingsRepository,
        super(SettingsInitialState(
          settings: UserSettingsEntity(
            fullName: "",
            dateOfBirth: '',
            notifySales: false,
            notifyArrivals: false,
            notifyDelivery: false,
          ),
        )) {
    on<UpdateFullNameEvent>(_onFullNameChanged);
    on<UpdateDateOfBirthEvent>(_onDateOfBirthChanged);
    on<UpdateNotifySalesEvent>(_onNotifySalesChanged);
    on<UpdateNotifyArrivalsEvent>(_onNotifyArrivalsChanged);
    on<UpdateNotifyDeliveryEvent>(_onNotifyDeliveryChanged);
  }

  Future<void> _onFullNameChanged(
    UpdateFullNameEvent event,
    Emitter<SettingsState> emit,
  ) async {
    var newSettings = state.settings;

    newSettings?.fullName = event.fullName!;
    try {
      await _settingsRepository.updateFullName(newSettings!.fullName);
      emit(FullNameUpdatedState(settings: newSettings));
    } catch (error) {
      emit(ChangeSettingsErrorState(
          settings: newSettings, errorMessage: error.toString()));
    }
  }

  Future<void> _onDateOfBirthChanged(
    UpdateDateOfBirthEvent event,
    Emitter<SettingsState> emit,
  ) async {
    var newSettings = state.settings;

    newSettings!.dateOfBirth = event.dateOfBirth!;
    try {
      await _settingsRepository.updateDateOfBirth(newSettings.dateOfBirth);
      emit(DateOfBirthUpdatedState(settings: newSettings));
    } catch (error) {
      emit(ChangeSettingsErrorState(
          settings: newSettings, errorMessage: error.toString()));
    }
  }

  Future<void> _onNotifySalesChanged(
    UpdateNotifySalesEvent event,
    Emitter<SettingsState> emit,
  ) async {
    var newSettings = state.settings;

    newSettings!.notifySales = event.notifySales!;
    try {
      await _settingsRepository.updateNotifySales(newSettings.notifySales);
      emit(NotifySalesUpdatedState(settings: newSettings));
    } catch (error) {
      emit(ChangeSettingsErrorState(
          settings: newSettings, errorMessage: error.toString()));
    }
  }

  Future<void> _onNotifyArrivalsChanged(
    UpdateNotifyArrivalsEvent event,
    Emitter<SettingsState> emit,
  ) async {
    var newSettings = state.settings;

    newSettings!.notifyArrivals = event.notifyArrivals!;
    try {
      await _settingsRepository
          .updateNotifyArrivals(newSettings.notifyArrivals);
      emit(NotifyArrivalsUpdatedSate(settings: newSettings));
    } catch (error) {
      emit(ChangeSettingsErrorState(
          settings: newSettings, errorMessage: error.toString()));
    }
  }

  Future<void> _onNotifyDeliveryChanged(
    UpdateNotifyDeliveryEvent event,
    Emitter<SettingsState> emit,
  ) async {
    var newSettings = state.settings;

    newSettings!.notifyDelivery = event.notifyDelivery!;
    try {
      await _settingsRepository
          .updateNotifyDelivery(newSettings.notifyDelivery);
      emit(NotifyDeliveryUpdatedState(settings: newSettings));
    } catch (error) {
      emit(ChangeSettingsErrorState(
          settings: newSettings, errorMessage: error.toString()));
    }
  }
}
