import 'package:ecommerce/domain/entities/user/settings.dart';
import 'package:equatable/equatable.dart';

abstract class SettingsState extends Equatable {
  final UserSettingsEntity? settings;

  const SettingsState({this.settings});

  @override
  List<Object> get props => [settings!];
}

class SettingsInitialState extends SettingsState {
  const SettingsInitialState({UserSettingsEntity? settings}) : super(settings: settings);
}

class FullNameUpdatedState extends SettingsState {
  const FullNameUpdatedState({UserSettingsEntity? settings}) : super(settings: settings);
}

class DateOfBirthUpdatedState extends SettingsState {
  const DateOfBirthUpdatedState({UserSettingsEntity? settings}) : super(settings: settings);
}

class NotifySalesUpdatedState extends SettingsState {
  const NotifySalesUpdatedState({UserSettingsEntity? settings}) : super(settings: settings);
}

class NotifyArrivalsUpdatedSate extends SettingsState {
  const NotifyArrivalsUpdatedSate({UserSettingsEntity? settings}) : super(settings: settings);
}

class NotifyDeliveryUpdatedState extends SettingsState {
  const NotifyDeliveryUpdatedState({UserSettingsEntity? settings}) : super(settings: settings);
}

class ChangeSettingsErrorState extends SettingsState {
  final String? errorMessage;

  const ChangeSettingsErrorState({UserSettingsEntity? settings, this.errorMessage})
      : super(settings: settings);
  @override
  List<Object> get props => [settings!, errorMessage!];
}
