import 'package:bloc/bloc.dart';
import 'package:ecommerce/data/repositories/mock/password_mock_repository.dart';

import 'password_event.dart';
import 'password_state.dart';

class PasswordBloc extends Bloc<PasswordEvent, PasswordState> {
  final PasswordMockRepository _passwordRepository;

  PasswordBloc({required PasswordMockRepository passwordRepository})
      : _passwordRepository = passwordRepository,
        super(PasswordInitialState()) {
    on<ChangePasswordEvent>(_onChanged);
  }

  Future<void> _onChanged(
    ChangePasswordEvent event,
    Emitter<PasswordState> emit,
  ) async {
    if (event.currentPassword.isEmpty) {
      emit(EmptyCurrentPasswordState());
    } else if (event.newPassword.isEmpty) {
      emit(EmptyNewPasswordState());
    } else if (event.repeatNewPassword.isEmpty) {
      emit(EmptyRepeatPasswordState());
    } else if (event.newPassword != event.repeatNewPassword) {
      emit(PasswordMismatchState());
    } else if (event.newPassword.length < 6) {
      emit(InvalidNewPasswordState());
    } else {
      try {
        var currentPassword = await _passwordRepository.getCurrentPassword();
        if (event.currentPassword != currentPassword) {
          emit(IncorrectCurrentPasswordState());
        } else {
          try {
            await _passwordRepository.changePassword(event.newPassword);
            emit(PasswordChangedState());
          } catch (error) {
            emit(ChangedPasswordErrorState(errorMessage: error.toString()));
          }
        }
      } catch (error) {
        emit(ChangedPasswordErrorState(errorMessage: error.toString()));
      }
    }
  }
}
