import 'package:equatable/equatable.dart';

abstract class PasswordState extends Equatable {
  @override
  List<Object?> get props => [];
}

class PasswordInitialState extends PasswordState {}

class IncorrectCurrentPasswordState extends PasswordState {}

class EmptyCurrentPasswordState extends PasswordState {}

class EmptyNewPasswordState extends PasswordState {}

class EmptyRepeatPasswordState extends PasswordState {}

class PasswordMismatchState extends PasswordState {}

class InvalidNewPasswordState extends PasswordState {}

class PasswordChangedState extends PasswordState {}

class ChangedPasswordErrorState extends PasswordState {
  final String? errorMessage;

  ChangedPasswordErrorState({this.errorMessage}) : super();

  @override
  List<Object?> get props => [errorMessage];
}