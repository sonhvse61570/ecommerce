import 'package:ecommerce/presentation/features/profile/profile_event.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'profile_bloc.dart';
import 'profile_state.dart';
import 'views/order_details.dart';
import 'views/orders.dart';
import 'views/payment_methods.dart';
import 'views/profile.dart';
import 'views/promos.dart';
import 'views/reviews.dart';
import 'views/settings.dart';
import 'views/shipping_addresses.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: OpenFlutterScaffold(
        title: 'My Profile',
        body: BlocProvider<ProfileBloc>(
            create: (context) {
              return ProfileBloc()..add(ProfileStartEvent());
            },
            child: const ProfileWrapper()),
        bottomMenuIndex: 4,
      ),
    );
  }
}

class ProfileWrapper extends StatefulWidget {
  const ProfileWrapper({Key? key}) : super(key: key);

  @override
  _ProfileWrapperState createState() => _ProfileWrapperState();
}

class _ProfileWrapperState extends OpenFlutterWrapperState<ProfileWrapper> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
        bloc: BlocProvider.of<ProfileBloc>(context),
        builder: (BuildContext context, ProfileState state) {
          return getPageView(<Widget>[
            ProfileView(changeView: changePage),
            MyOrdersView(changeView: changePage),
            MyShippingAddressesView(changeView: changePage),
            PaymentMethodsView(changeView: changePage),
            PromosView(changeView: changePage),
            MyReviewsView(changeView: changePage),
            SettingsView(changeView: changePage),
            MyOrderDetailsView(changeView: changePage),
          ]);
        });
  }
}
