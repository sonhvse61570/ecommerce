import 'package:bloc/bloc.dart';
import 'package:ecommerce/data/model/favorite_product.dart';
import 'package:ecommerce/domain/usecases/cart/change_cart_item_quantity_use_case.dart';
import 'package:ecommerce/domain/usecases/cart/get_cart_products_use_case.dart';
import 'package:ecommerce/domain/usecases/cart/remove_product_from_cart_use_case.dart';
import 'package:ecommerce/domain/usecases/favorites/add_to_favorites_use_case.dart';
import 'package:ecommerce/domain/usecases/promos/get_promos_use_case.dart';
import 'package:ecommerce/locator.dart';

import 'cart_event.dart';
import 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  final GetCartProductsUseCase _getCartProductsUseCase;
  final RemoveProductFromCartUseCase _removeProductFromCartUseCase;
  final AddToFavoritesUseCase _addToFavoritesUseCase;
  final GetPromosUseCase _getPromosUseCase;

  CartBloc()
      : _getCartProductsUseCase = sl(),
        _removeProductFromCartUseCase = sl(),
        _addToFavoritesUseCase = sl(),
        _getPromosUseCase = sl(),
        super(CartInitialState()) {
    on<CartLoadedEvent>(_onCartFetched);
    on<CartQuantityChangedEvent>(_onCartQuantityChanged);
    on<CartRemoveFromCartEvent>(_onCartRemoved);
    on<CartAddToFavsEvent>(_onFavoriteAdded);
    on<CartPromoAppliedEvent>(_onPromoApplied);
    on<CartPromoCodeAppliedEvent>(_onPromoCodeApplied);
    on<CartShowPopupEvent>(_onPopupShowed);
  }

  Future<void> _onCartFetched(
    CartLoadedEvent event,
    Emitter<CartState> emit,
  ) async {
    if (state is CartInitialState) {
      final cartResults = await _getCartProductsUseCase.execute(GetCartProductParams());
      var promos = await _getPromosUseCase.execute(GetPromosParams());

      emit(CartLoadedState(
          showPromoPopup: false,
          totalPrice: cartResults.totalPrice,
          calculatedPrice: cartResults.calculatedPrice,
          promos: promos.promos,
          appliedPromo: cartResults.appliedPromo,
          cartProducts: cartResults.cartItems));
    } else {
      emit(state);
    }
  }

  Future<void> _onCartQuantityChanged(
    CartQuantityChangedEvent event,
    Emitter<CartState> emit,
  ) async {
    var state = this.state as CartLoadedState;
    emit(CartLoadingState());
    ChangeCartItemQuantityUseCase changeCartItemQuantityUseCase = sl();
    if (event.newQuantity < 1) {
      await _removeProductFromCartUseCase.execute(event.item);
    } else {
      await changeCartItemQuantityUseCase.execute(ChangeCartItemQuantityParams(
        item: event.item,
        quantity: event.newQuantity,
      ));
    }

    final cartResults = await _getCartProductsUseCase
        .execute(GetCartProductParams(appliedPromo: null));
    emit(CartLoadedState(
      cartProducts: cartResults.cartItems,
      promos: state.promos,
      showPromoPopup: state.showPromoPopup,
      totalPrice: cartResults.cartItems.isEmpty ? 0.0 : cartResults.totalPrice,
    ));
  }

  Future<void> _onCartRemoved(
    CartRemoveFromCartEvent event,
    Emitter<CartState> emit,
  ) async {
    var state = this.state as CartLoadedState;
    emit(CartLoadingState());
    await _removeProductFromCartUseCase.execute(event.item);
    final cartResults = await _getCartProductsUseCase
        .execute(GetCartProductParams(appliedPromo: null));
    emit(CartLoadedState(
      cartProducts: cartResults.cartItems,
      promos: state.promos,
      showPromoPopup: state.showPromoPopup,
      totalPrice: cartResults.cartItems.isEmpty ? 0.0 : cartResults.totalPrice,
    ));
  }

  Future<void> _onFavoriteAdded(
    CartAddToFavsEvent event,
    Emitter<CartState> emit,
  ) async {
    await _addToFavoritesUseCase.execute(FavoriteProduct(
      event.item.product,
      event.item.selectedAttributes,
    ));
  }

  Future<void> _onPromoApplied(
    CartPromoAppliedEvent event,
    Emitter<CartState> emit,
  ) async {
    var state = this.state as CartLoadedState;
    final cartResults = await _getCartProductsUseCase
        .execute(GetCartProductParams(appliedPromo: event.promo));
    emit(state.copyWith(
      showPromoPopup: false,
      totalPrice: cartResults.totalPrice,
      calculatedPrice: cartResults.calculatedPrice,
      appliedPromo: event.promo,
    ));
  }

  Future<void> _onPromoCodeApplied(
    CartPromoCodeAppliedEvent event,
    Emitter<CartState> emit,
  ) async {
    var state = this.state as CartLoadedState;
    emit(state.copyWith(showPromoPopup: false));
  }

  Future<void> _onPopupShowed(
    CartShowPopupEvent event,
    Emitter<CartState> emit,
  ) async {
    var state = this.state as CartLoadedState;
    emit(state.copyWith(showPromoPopup: true));
  }
}
