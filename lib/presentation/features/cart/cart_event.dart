import 'package:ecommerce/data/model/cart_item.dart';
import 'package:ecommerce/data/model/promo.dart';
import 'package:equatable/equatable.dart';

abstract class CartEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class CartLoadedEvent extends CartEvent {}

class CartQuantityChangedEvent extends CartEvent {
  final CartItem item;
  final int newQuantity;

  CartQuantityChangedEvent({required this.item, required this.newQuantity});
}

class CartRemoveFromCartEvent extends CartEvent {
  final CartItem item;

  CartRemoveFromCartEvent({required this.item});
}

class CartAddToFavsEvent extends CartEvent {
  final CartItem item;

  CartAddToFavsEvent({required this.item});
}

class CartPromoAppliedEvent extends CartEvent {
  final Promo promo;

  CartPromoAppliedEvent({required this.promo});
}

class CartPromoCodeAppliedEvent extends CartEvent {
  final String promoCode;

  CartPromoCodeAppliedEvent({required this.promoCode});
}

class CartShowPopupEvent extends CartEvent {}