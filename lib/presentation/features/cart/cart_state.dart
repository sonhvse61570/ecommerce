import 'package:ecommerce/data/model/cart_item.dart';
import 'package:ecommerce/data/model/promo.dart';
import 'package:equatable/equatable.dart';

abstract class CartState extends Equatable {
  @override
  List<Object?> get props => [];
}

class CartInitialState extends CartState {}

class CartLoadingState extends CartState {}

class CartErrorState extends CartState {}

class CartLoadedState extends CartState {
  final List<CartItem> cartProducts;
  final List<Promo> promos;
  final bool showPromoPopup;
  final double? totalPrice;
  final double? calculatedPrice;
  final Promo? appliedPromo;

  CartLoadedState({
    required this.cartProducts,
    required this.promos,
    required this.showPromoPopup,
    this.totalPrice,
    this.calculatedPrice,
    this.appliedPromo,
  });

  CartLoadedState copyWith(
      {List<CartItem>? cartProducts,
        double? totalPrice,
        List<Promo>? promos,
        Promo? appliedPromo,
        double? calculatedPrice,
        bool? showPromoPopup}) {
    return CartLoadedState(
        promos: promos ?? this.promos,
        cartProducts: cartProducts ?? this.cartProducts,
        totalPrice: totalPrice ?? this.totalPrice,
        calculatedPrice: calculatedPrice ?? this.calculatedPrice,
        appliedPromo: appliedPromo ?? this.appliedPromo,
        showPromoPopup: showPromoPopup ?? this.showPromoPopup);
  }

  @override
  List<Object?> get props => [totalPrice!, cartProducts, appliedPromo, showPromoPopup];
}
