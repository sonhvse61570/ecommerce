import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cart_bloc.dart';
import 'cart_event.dart';
import 'cart_state.dart';
import 'views/cart_view.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: OpenFlutterScaffold(
        background: null,
        title: null,
        body: BlocProvider<CartBloc>(
          create: (context) {
            return CartBloc()..add(CartLoadedEvent());
          },
          child: const _CartWrapper(),
        ),
        bottomMenuIndex: 2,
      ),
    );
  }
}

class _CartWrapper extends StatefulWidget {
  const _CartWrapper({Key? key}) : super(key: key);

  @override
  _CartWrapperState createState() => _CartWrapperState();
}

class _CartWrapperState extends OpenFlutterWrapperState<_CartWrapper> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartBloc, CartState>(
        builder: (BuildContext context, CartState state) {
      return getPageView(<Widget>[CartView(changeView: changePage)]);
    });
  }
}
