import 'package:ecommerce/data/model/favorite_product.dart';
import 'package:ecommerce/data/model/filter_rules.dart';
import 'package:ecommerce/data/model/hashtag.dart';
import 'package:ecommerce/data/model/sort_rules.dart';
import 'package:equatable/equatable.dart';

class FavoritesState extends Equatable {
  final List<FavoriteProduct>? data;
  final List<HashTag>? hashTags;
  final SortRules? sortBy;
  final FilterRules? filterRules;
  final String? error;
  final bool isList;

  const FavoritesState(
      {this.data,
      this.hashTags,
      this.sortBy,
      this.filterRules,
      this.error,
      this.isList = true});

  FavoritesState copyWith({
    List<FavoriteProduct>? data,
    List<HashTag>? hashtags,
    SortRules? sortBy,
    FilterRules? filterRules,
    String? error,
    bool? isList,
  }) {
    return FavoritesState(
        data: data ?? this.data,
        hashTags: hashtags ?? hashTags,
        sortBy: sortBy ?? this.sortBy,
        filterRules: filterRules ?? this.filterRules,
        isList: isList ?? this.isList);
  }

  bool get isProductsLoading => data == null;

  bool get isFilterRulesVisible => filterRules != null;

  bool get hasError => error != null;

  FavoritesState getLoading() {
    return copyWith(data: []);
  }

  @override
  List<Object?> get props =>
      [data, hashTags, sortBy, filterRules, error, isProductsLoading];
}
