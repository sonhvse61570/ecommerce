import 'package:ecommerce/config/app_routes.dart';
import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/features/favorites/favorites_bloc.dart';
import 'package:ecommerce/presentation/features/favorites/favorites_event.dart';
import 'package:ecommerce/presentation/features/favorites/favorites_state.dart';
import 'package:ecommerce/presentation/features/product_details/product_details_screen.dart';
import 'package:ecommerce/presentation/widgets/extensions/product_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FavoritesTileView extends StatelessWidget {
  const FavoritesTileView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FavoritesBloc, FavoritesState>(
        builder: (context, state) {
          return SliverGrid(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 4.0,
                crossAxisSpacing: 4.0,
                childAspectRatio: 1.589),
            delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: AppSizes.sidePadding),
                  child: state.data![index].getTileView(
                      context: context,
                      showProductInfo: () {
                        Navigator.of(context).pushNamed(
                            AppRoutes.product,
                            arguments: ProductDetailsParameters(
                                state.data![index].product.id,
                                state.data![index].product.categories.isNotEmpty ?
                                state.data![index].product.categories[0].id: 0,
                                selectedAttributes: state.data![index].favoriteForm));
                      },
                      onRemoveFromFavorites: () {
                        BlocProvider.of<FavoritesBloc>(context).add(
                            RemoveFromFavoriteEvent(state.data![index]));
                      },
                      onAddToCart: () {
                        BlocProvider.of<FavoritesBloc>(context)
                            .add(AddToCartEvent(state.data![index]));
                        Navigator.of(context)
                            .pushNamed(AppRoutes.cart);
                      },
                      selectedAttributes: state.data![index].favoriteForm
                  ),
                );
              },
            ),
          );
        });
  }
}
