import 'package:ecommerce/presentation/features/favorites/favorites_bloc.dart';
import 'package:ecommerce/presentation/features/favorites/favorites_state.dart';
import 'package:ecommerce/presentation/widgets/data_driven/size_changing_app_bar.dart';
import 'package:ecommerce/presentation/widgets/independent/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'favorites_event.dart';
import 'views/favorites_list_view.dart';
import 'views/favorites_tile_view.dart';

class FavoritesScreen extends StatefulWidget {
  const FavoritesScreen({Key? key}) : super(key: key);

  @override
  State<FavoritesScreen> createState() => _FavoritesScreenState();
}

class _FavoritesScreenState extends State<FavoritesScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: OpenFlutterScaffold(
        background: null,
        title: null,
        body: BlocProvider<FavoritesBloc>(
          create: (context) {
            return FavoritesBloc()..add(FavoriteLoadedEvent());
          },
          child: _buildScreen(context),
        ),
        bottomMenuIndex: 3,
      ),
    );
  }

  Widget _buildScreen(BuildContext context) {
    return BlocBuilder<FavoritesBloc, FavoritesState>(
      builder: (BuildContext context, FavoritesState state) {
        return CustomScrollView(
          slivers: <Widget>[
            SizeChangingAppBar(
              title: 'Favorites',
              filterRules: state.filterRules,
              sortRules: state.sortBy,
              isListView: state.isList,
              onFilterRulesChanged: (filter) {
                BlocProvider.of<FavoritesBloc>(context).add(ProductChangeFilterRulesEvent(filter));
              },
              onSortRulesChanged: (sort) {
                BlocProvider.of<FavoritesBloc>(context)
                    .add(ProductChangeSortRulesEvent(sort));
              },
              onViewChanged: () {
                BlocProvider.of<FavoritesBloc>(context)
                    .add(ProductsChangeViewEvent());
              },
            ),
            state.isList ? const FavoritesListView() : const FavoritesTileView()
          ],
        );
      },
    );
  }
}
