import 'package:bloc/bloc.dart';
import 'package:ecommerce/data/model/cart_item.dart';
import 'package:ecommerce/data/model/sort_rules.dart';
import 'package:ecommerce/domain/usecases/cart/add_product_to_cart_use_case.dart';
import 'package:ecommerce/domain/usecases/favorites/get_favorite_products_use_case.dart';
import 'package:ecommerce/domain/usecases/favorites/remove_from_favorites_use_case.dart';
import 'package:ecommerce/locator.dart';
import 'package:ecommerce/presentation/features/favorites/favorites_event.dart';
import 'package:ecommerce/presentation/features/favorites/favorites_state.dart';

class FavoritesBloc extends Bloc<FavoritesEvent, FavoritesState> {
  final RemoveFromFavoritesUseCase _removeFromFavoritesUseCase;
  final GetFavoriteProductsUseCase _getFavoriteProductsUseCase;
  final AddProductToCartUseCase _addProductToCartUseCase;

  FavoritesBloc()
      : _removeFromFavoritesUseCase = sl(),
        _getFavoriteProductsUseCase = sl(),
        _addProductToCartUseCase = sl(),
        super(const FavoritesState()) {
    on<FavoriteLoadedEvent>(_onFetched);
    on<ProductsChangeViewEvent>(_onViewChanged);
    on<ProductChangeSortRulesEvent>(_onSortRulesChanged);
    on<ProductChangeFilterRulesEvent>(_onFilterRulesChanged);
    on<AddToCartEvent>(_onCartAdded);
    on<RemoveFromFavoriteEvent>(_onFavoriteRemoved);
  }

  Future<void> _onFetched(
    FavoriteLoadedEvent event,
    Emitter<FavoritesState> emit,
  ) async {
    GetFavoriteProductResult favoriteProducts =
        await _getFavoriteProductsUseCase.execute(GetFavoriteProductParams());

    emit(FavoritesState(
      sortBy: const SortRules(),
      data: favoriteProducts.products,
      filterRules: favoriteProducts.filterRules,
    ));
  }

  Future<void> _onViewChanged(
    ProductsChangeViewEvent event,
    Emitter<FavoritesState> emit,
  ) async {
    emit(state.copyWith(isList: !state.isList));
  }

  Future<void> _onSortRulesChanged(
    ProductChangeSortRulesEvent event,
    Emitter<FavoritesState> emit,
  ) async {
    emit(state.getLoading());
    final filteredData = await _getFavoriteProductsUseCase.execute(
      GetFavoriteProductParams(
        filterRules: state.filterRules,
        sortRules: event.sortBy,
      ),
    );
    emit(state.copyWith(
      sortBy: event.sortBy,
      data: filteredData.products,
    ));
  }

  Future<void> _onFilterRulesChanged(
    ProductChangeFilterRulesEvent event,
    Emitter<FavoritesState> emit,
  ) async {
    emit(state.getLoading());
    final filteredData =
        await _getFavoriteProductsUseCase.execute(GetFavoriteProductParams(
      filterRules: event.filterRules,
      sortRules: state.sortBy!,
    ));
    emit(state.copyWith(
      filterRules: event.filterRules,
      data: filteredData.products,
    ));
  }

  Future<void> _onCartAdded(
    AddToCartEvent event,
    Emitter<FavoritesState> emit,
  ) async {
    await _addProductToCartUseCase.execute(CartItem(
      product: event.favoriteProduct.product,
      productQuantity: ProductQuantity(1),
      selectedAttributes: event.favoriteProduct.favoriteForm,
    ));
  }

  Future<void> _onFavoriteRemoved(
    RemoveFromFavoriteEvent event,
    Emitter<FavoritesState> emit,
  ) async {
    emit(state.getLoading());

    await _removeFromFavoritesUseCase
        .execute(RemoveFromFavoritesParams(event.product));
    final filteredData =
        await _getFavoriteProductsUseCase.execute(GetFavoriteProductParams(
      filterRules: state.filterRules,
      sortRules: state.sortBy!,
    ));
    emit(state.copyWith(data: filteredData.products));
  }
}
