import 'package:ecommerce/data/model/favorite_product.dart';
import 'package:ecommerce/data/model/filter_rules.dart';
import 'package:ecommerce/data/model/sort_rules.dart';
import 'package:equatable/equatable.dart';

abstract class FavoritesEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class FavoriteLoadedEvent extends FavoritesEvent {

  FavoriteLoadedEvent();

  @override
  List<Object?> get props => [];
}

class ProductsChangeViewEvent extends FavoritesEvent {}

class ProductChangeSortRulesEvent extends FavoritesEvent {
  final SortRules sortBy;

  ProductChangeSortRulesEvent(this.sortBy);

  @override
  List<Object?> get props => [sortBy];
}

class ProductChangeFilterRulesEvent extends FavoritesEvent {
  final FilterRules filterRules;

  ProductChangeFilterRulesEvent(this.filterRules);

  @override
  List<Object?> get props => [filterRules];
}

class SearchEvent extends FavoritesEvent {
  final String search;

  SearchEvent(this.search);

  @override
  List<Object?> get props => [search];
}

class RemoveFromFavoriteEvent extends FavoritesEvent {
  final FavoriteProduct product;

  RemoveFromFavoriteEvent(this.product);

  @override
  List<Object?> get props => [product];
}

class AddToCartEvent extends FavoritesEvent {
  final FavoriteProduct favoriteProduct;

  AddToCartEvent(this.favoriteProduct);

  @override
  List<Object?> get props => [favoriteProduct];
}