import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_bloc.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_event.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_state.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_button.dart';
import 'package:ecommerce/presentation/widgets/independent/input_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddShippingAddressView extends StatefulWidget {
  const AddShippingAddressView({Key? key, this.changeView}) : super(key: key);

  final Function? changeView;

  @override
  State<AddShippingAddressView> createState() => _AddShippingAddressViewState();
}

class _AddShippingAddressViewState extends State<AddShippingAddressView> {
  int paymentCardIndex = 0;

  late TextEditingController _fullNameController;
  late TextEditingController _addressController;
  late TextEditingController _cityController;
  late TextEditingController _stateController;
  late TextEditingController _postalController;
  late TextEditingController _countryController;

  @override
  void initState() {
    _fullNameController = TextEditingController();
    _addressController = TextEditingController();
    _cityController = TextEditingController();
    _stateController = TextEditingController();
    _postalController = TextEditingController();
    _countryController = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    _fullNameController.dispose();
    _addressController.dispose();
    _cityController.dispose();
    _stateController.dispose();
    _postalController.dispose();
    _countryController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<CheckoutBloc>(context);
    var _theme = Theme.of(context);

    return BlocListener(
      bloc: bloc,
      listener: (BuildContext context, CheckoutState state) {
        if (state is CheckoutErrorState) {
          Container(
              padding: const EdgeInsets.all(AppSizes.sidePadding),
              child: Text('An error occured',
                  style: _theme.textTheme.headline4
                      ?.copyWith(color: _theme.errorColor)));
        }
      },
      child: BlocBuilder(
        bloc: bloc,
        builder: (BuildContext context, CheckoutState state) {
          if (state is CheckoutProceedState) {}

          return SingleChildScrollView(
            child: Column(
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.only(bottom: AppSizes.sidePadding),
                ),
                OpenFlutterInputField(
                  controller: _fullNameController,
                  hint: 'Full name',
                ),
                const Padding(
                  padding: EdgeInsets.only(bottom: AppSizes.sidePadding),
                ),
                OpenFlutterInputField(
                  controller: _addressController,
                  hint: 'Address',
                ),
                const Padding(
                  padding: EdgeInsets.only(bottom: AppSizes.sidePadding),
                ),
                OpenFlutterInputField(
                  controller: _cityController,
                  hint: 'City',
                ),
                const Padding(
                  padding: EdgeInsets.only(bottom: AppSizes.sidePadding),
                ),
                OpenFlutterInputField(
                  controller: _stateController,
                  hint: 'State/Province/Region',
                ),
                const Padding(
                  padding: EdgeInsets.only(bottom: AppSizes.sidePadding),
                ),
                OpenFlutterInputField(
                  controller: _postalController,
                  hint: 'Zip Code (Postal Code)',
                ),
                const Padding(
                  padding: EdgeInsets.only(bottom: AppSizes.sidePadding),
                ),
                OpenFlutterInputField(
                  controller: _countryController,
                  hint: 'Country',
                ),
                const Padding(
                  padding: EdgeInsets.only(bottom: AppSizes.sidePadding),
                ),
                OpenFlutterButton(
                  title: 'SAVE ADDRESS',
                  onPressed: (() => {
                        bloc
                          ..add(CheckoutAddNewShippingAddressEvent(
                              fullName: _fullNameController.text,
                              address: _addressController.text,
                              city: _cityController.text,
                              state: _stateController.text,
                              postal: _postalController.text,
                              country: _countryController.text)),
                        widget.changeView!(changeType: ViewChangeType.Backward)
                      }),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
