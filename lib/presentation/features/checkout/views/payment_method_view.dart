import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_bloc.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_state.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/block_subtitle.dart';
import 'package:ecommerce/presentation/widgets/independent/bottom_popup.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_button.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_checkbox.dart';
import 'package:ecommerce/presentation/widgets/independent/input_field.dart';
import 'package:ecommerce/presentation/widgets/independent/payment_card_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../checkout_event.dart';

class PaymentMethodView extends StatefulWidget {
  const PaymentMethodView({Key? key, this.changeView}) : super(key: key);

  final Function? changeView;

  @override
  State<PaymentMethodView> createState() => _PaymentMethodViewState();
}

class _PaymentMethodViewState extends State<PaymentMethodView> {
  int paymentCardIndex = 0;

  late TextEditingController _nameOnCardController;
  late TextEditingController _cardNumberController;
  late TextEditingController _expirationDateController;
  late TextEditingController _cvvController;

  @override
  void initState() {
    _nameOnCardController = TextEditingController();
    _cardNumberController = TextEditingController();
    _expirationDateController = TextEditingController();
    _cvvController = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    _nameOnCardController.dispose();
    _cardNumberController.dispose();
    _expirationDateController.dispose();
    _cvvController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<CheckoutBloc>(context);
    var _theme = Theme.of(context);
    var width = MediaQuery.of(context).size.width;

    return BlocListener(
        bloc: bloc,
        listener: (BuildContext context, CheckoutState state) {
          if (state is CheckoutErrorState) {
            Container(
                padding: const EdgeInsets.all(AppSizes.sidePadding),
                child: Text('An error occur',
                    style: _theme.textTheme.headline4
                        ?.copyWith(color: _theme.errorColor)));
          }
        },
        child: BlocBuilder(
            bloc: bloc,
            builder: (BuildContext context, CheckoutState state) {
              var currentCardId = 0;
              var showAddNewCardForm = false;
              if (state is CheckoutProceedState) {
                currentCardId = state.cardId;
                showAddNewCardForm = state.showAddNewCardForm;
              }

              return SingleChildScrollView(
                  child: Stack(children: <Widget>[
                Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: AppSizes.sidePadding),
                    child: Column(children: <Widget>[
                      OpenFlutterBlockSubtitle(
                          width: width, title: 'Your payment cards'),
                      OpenFlutterPaymentCardPreview(
                        width: width,
                        cardNumber: '**** **** **** 3947',
                        cardHolderName: 'Jennyfer Doe',
                        expirationMonth: 5,
                        expirationYear: 23,
                      ),
                      OpenFlutterCheckbox(
                          width: width,
                          title: 'Use as default payment method',
                          checked: currentCardId == 1,
                          onTap: ((bool newValue) =>
                              {_changeDefaultPaymentCard(bloc, 1)})),
                      OpenFlutterPaymentCardPreview(
                          width: width,
                          cardNumber: '**** **** **** 4546',
                          cardHolderName: 'Jennyfer Doe',
                          expirationMonth: 11,
                          expirationYear: 22,
                          cardType: CardType.Visa),
                      OpenFlutterCheckbox(
                          width: width,
                          title: 'Use as default payment method',
                          checked: currentCardId == 2,
                          onTap: ((bool newValue) =>
                              {_changeDefaultPaymentCard(bloc, 2)})),
                    ])),
                Positioned(
                  bottom: AppSizes.sidePadding,
                  right: AppSizes.sidePadding,
                  child: FloatingActionButton(
                      mini: true,
                      backgroundColor: _theme.primaryColor,
                      onPressed: (() =>
                          {bloc..add(CheckoutShowAddNewCardEvent())}),
                      child: const Icon(Icons.add, size: 36)),
                ),
                showAddNewCardForm
                    ? OpenFlutterBottomPopup(
                        title: 'Add new card',
                        //height: 550,
                        child: Column(
                          children: <Widget>[
                            OpenFlutterInputField(
                              controller: _nameOnCardController,
                              hint: 'Name on card',
                            ),
                            const Padding(
                              padding:
                                  EdgeInsets.only(bottom: AppSizes.sidePadding),
                            ),
                            OpenFlutterInputField(
                              controller: _cardNumberController,
                              hint: 'Card number',
                            ),
                            const Padding(
                              padding:
                                  EdgeInsets.only(bottom: AppSizes.sidePadding),
                            ),
                            OpenFlutterInputField(
                              controller: _expirationDateController,
                              hint: 'Expiration Date',
                            ),
                            const Padding(
                              padding:
                                  EdgeInsets.only(bottom: AppSizes.sidePadding),
                            ),
                            OpenFlutterInputField(
                              controller: _cvvController,
                              hint: 'CVV',
                            ),
                            const Padding(
                              padding:
                                  EdgeInsets.only(bottom: AppSizes.sidePadding),
                            ),
                            OpenFlutterCheckbox(
                                width: width,
                                title: 'Use as default payment method',
                                checked: false,
                                onTap: ((bool newValue) =>
                                    {_changeDefaultPaymentCard(bloc, 3)})),
                            const Padding(
                              padding:
                                  EdgeInsets.only(bottom: AppSizes.sidePadding),
                            ),
                            OpenFlutterButton(
                              title: 'ADD CARD',
                              onPressed: (() => {
                                    bloc
                                      ..add(CheckoutAddNewCardEvent(
                                          nameOnCard:
                                              _nameOnCardController.text,
                                          cardNumber:
                                              _cardNumberController.text,
                                          expirationMonth: int.parse(
                                              _expirationDateController.text
                                                  .split('/')[0]),
                                          expirationYear: int.parse(
                                              _expirationDateController.text
                                                  .split('/')[1]),
                                          cvv: int.parse(_cvvController.text)))
                                  }),
                            )
                          ],
                        ),
                      )
                    : Container()
              ]));
            }));
  }

  void _changeDefaultPaymentCard(CheckoutBloc bloc, int cardId) {
    bloc.add(CheckoutSetDefaultCardEvent(cardId));
    widget.changeView!(changeType: ViewChangeType.Exact, index: 0);
    //TODO: implement change of default payment card
  }
}
