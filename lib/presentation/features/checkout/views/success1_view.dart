import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_button.dart';
import 'package:flutter/material.dart';

class Success1View extends StatefulWidget {
  const Success1View({Key? key, required this.changeView}) : super(key: key);

  final Function changeView;

  @override
  State<Success1View> createState() => _Success1ViewState();
}

class _Success1ViewState extends State<Success1View> {
  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);

    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppSizes.imageRadius),
            image: const DecorationImage(fit: BoxFit.fitHeight, image: AssetImage('assets/images/checkout/success.png'))),
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: AppSizes.sidePadding * 3),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: AppSizes.sidePadding * 3),
                  child: Text('Success!', style: _theme.textTheme.caption),
                ),
                Padding(
                    padding: const EdgeInsets.all(AppSizes.sidePadding),
                    child: Text('Your order will be delivered soon. Thank you for choosing our app!',
                        style: _theme.textTheme.headline4)),
                OpenFlutterButton(
                  title: 'Continue shopping',
                  onPressed: (() => {widget.changeView(changeType: ViewChangeType.Forward)}),
                ),
              ],
            )));
  }
}
