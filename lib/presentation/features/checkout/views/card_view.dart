import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_bloc.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_state.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/action_card.dart';
import 'package:ecommerce/presentation/widgets/independent/block_subtitle.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_button.dart';
import 'package:ecommerce/presentation/widgets/independent/delivery_method.dart';
import 'package:ecommerce/presentation/widgets/independent/payment_card.dart';
import 'package:ecommerce/presentation/widgets/independent/summary_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartView extends StatefulWidget {
  const CartView({Key? key, this.changeView}) : super(key: key);

  final Function? changeView;

  @override
  State<CartView> createState() => _CartViewState();
}

class _CartViewState extends State<CartView> {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<CheckoutBloc>(context);
    var _theme = Theme.of(context);
    var width = MediaQuery.of(context).size.width - AppSizes.sidePadding * 2;

    return BlocListener(
        bloc: bloc,
        listener: (context, state) {
          if (state is CheckoutErrorState) {
            Container(
                padding: const EdgeInsets.all(AppSizes.sidePadding),
                child: Text('An error occured',
                    style: _theme.textTheme.headline4
                        ?.copyWith(color: _theme.errorColor)));
          }
        },
        child: BlocBuilder(
            bloc: bloc,
            builder: (BuildContext context, CheckoutState state) {
              if (state is CheckoutProceedState) {
                return SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        OpenFlutterBlockSubtitle(
                            title: 'Shipping Address', width: width),
                        OpenFlutterActionCard(
                            title: state.currentShippingAddress?.fullName ?? "",
                            linkText: 'Change',
                            onLinkTap: (() => {
                              widget.changeView!(
                                  changeType: ViewChangeType.Exact, index: 2)
                            }),
                            child: RichText(
                              text: TextSpan(
                                  text: state.currentShippingAddress.toString(),
                                  style: _theme.textTheme.headline2
                                      ?.copyWith(color: _theme.primaryColor)),
                              maxLines: 2,
                            )),
                        OpenFlutterBlockSubtitle(
                          title: 'Payment',
                          width: width,
                          linkText: 'Change',
                          onLinkTap: (() => {
                            widget.changeView!(
                                changeType: ViewChangeType.Forward)
                          }),
                        ),
                        OpenFlutterPaymentCard(
                          cardNumber: state.currentPaymentMethod.toString(),
                        ),
                        OpenFlutterBlockSubtitle(
                          title: 'Delivery Method',
                          width: width,
                          /*linkText: 'Change',
                      onLinkTap: (() => {}),*/
                        ),
                        const OpenFlutterDeliveryMethod(),
                        const Padding(
                            padding:
                            EdgeInsets.only(top: AppSizes.sidePadding * 3)),
                        OpenFlutterSummaryLine(
                            title: 'Order',
                            summary: '\$' + state.orderPrice.toStringAsFixed(2)),
                        const Padding(
                            padding: EdgeInsets.only(top: AppSizes.sidePadding)),
                        OpenFlutterSummaryLine(
                            title: 'Delivery',
                            summary: '\$' + state.deliveryPrice.toStringAsFixed(2)),
                        const Padding(
                            padding: EdgeInsets.only(top: AppSizes.sidePadding)),
                        OpenFlutterSummaryLine(
                            title: 'Summary',
                            summary: '\$' + state.summaryPrice.toStringAsFixed(2)),
                        const Padding(
                            padding: EdgeInsets.only(top: AppSizes.sidePadding)),
                        OpenFlutterButton(
                          title: 'SUBMIT ORDER',
                          onPressed: (() => {
                            widget.changeView!(
                                changeType: ViewChangeType.Exact, index: 4)
                          }),
                        )
                      ],
                    ));
              }
              return Container();
            }));
  }
}
