import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_button.dart';
import 'package:flutter/material.dart';

class Success2View extends StatefulWidget {
  const Success2View({Key? key, required this.changeView}) : super(key: key);

  final Function changeView;

  @override
  State<Success2View> createState() => _Success2ViewState();
}

class _Success2ViewState extends State<Success2View> {
  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: AppSizes.sidePadding),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
                padding: const EdgeInsets.only(top: AppSizes.sidePadding * 5),
                child: Image.asset('assets/images/checkout/bags.png')),
            Padding(
              padding: const EdgeInsets.only(
                top: AppSizes.sidePadding * 3,
                left: AppSizes.sidePadding * 2,
                right: AppSizes.sidePadding * 2,
              ),
              child: Text('Success!', style: _theme.textTheme.caption),
            ),
            Padding(
                padding: const EdgeInsets.all(AppSizes.sidePadding * 2),
                child: Text('Your order will be delivered soon. Thank you for choosing our app!',
                    style: _theme.textTheme.headline4)),
            OpenFlutterButton(
              title: 'CONTINUE SHOPPING',
              onPressed: (() => {widget.changeView(changeType: ViewChangeType.Exact, index: 0)}),
            ),
          ],
        ));
  }
}
