import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_bloc.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_event.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_state.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/action_card.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_checkbox.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShippingAddressView extends StatefulWidget {
  const ShippingAddressView({Key? key, this.changeView}) : super(key: key);

  final Function? changeView;

  @override
  State<ShippingAddressView> createState() => _ShippingAddressViewState();
}

class _ShippingAddressViewState extends State<ShippingAddressView> {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<CheckoutBloc>(context);
    var _theme = Theme.of(context);
    var width = MediaQuery.of(context).size.width - AppSizes.sidePadding * 2;

    return BlocListener(
        bloc: bloc,
        listener: (context, state) {
          if (state is CheckoutErrorState) {
            Container(
                padding: const EdgeInsets.all(AppSizes.sidePadding),
                child: Text('An error occured', style: _theme.textTheme.headline4?.copyWith(color: _theme.errorColor)));
          }
        },
        child: BlocBuilder(
            bloc: bloc,
            builder: (BuildContext context, CheckoutState state) {
              return SingleChildScrollView(
                  child: Stack(children: <Widget>[
                    Column(
                      children: <Widget>[
                        _buildShippingAddress(_theme, width, bloc, true),
                        _buildShippingAddress(_theme, width, bloc, false),
                        _buildShippingAddress(_theme, width, bloc, false),
                      ],
                    ),
                    Positioned(
                      bottom: AppSizes.sidePadding,
                      right: AppSizes.sidePadding,
                      child: FloatingActionButton(
                          mini: true,
                          backgroundColor: _theme.primaryColor,
                          onPressed: (() => {widget.changeView!(changeType: ViewChangeType.Forward)}),
                          child: const Icon(Icons.add, size: 36)),
                    ),
                  ]));
            }));
  }

  Widget _buildShippingAddress(ThemeData _theme, double width, CheckoutBloc bloc, bool checked) {
    return OpenFlutterActionCard(
        title: 'Jane Doe',
        linkText: 'Edit',
        onLinkTap: (() => {widget.changeView!(changeType: ViewChangeType.Forward)}),
        child: Column(children: <Widget>[
          RichText(
            text: TextSpan(
                text: '3 Newbridge Court Chino Hills, CA 91709, United States',
                style: _theme.textTheme.headline2?.copyWith(color: _theme.primaryColor)),
            maxLines: 2,
          ),
          Container(
              width: width,
              alignment: Alignment.centerRight,
              child: OpenFlutterCheckbox(
                  width: width,
                  title: 'Use as the shipping address',
                  checked: checked,
                  onTap: ((bool newValue) => {_changeDefaultShippingAddress(bloc, 3)}))),
        ]));
  }

  void _changeDefaultShippingAddress(CheckoutBloc bloc, int shippingAddressId) {
    //TODO: pass data to state
    bloc.add(CheckoutSetDefaultShippingAddressEvent(shippingAddressId));
    widget.changeView!(changeType: ViewChangeType.Exact, index: 0);
  }
}
