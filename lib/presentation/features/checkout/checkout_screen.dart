import 'package:ecommerce/presentation/features/checkout/checkout_bloc.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_event.dart';
import 'package:ecommerce/presentation/features/checkout/checkout_state.dart';
import 'package:ecommerce/presentation/features/checkout/views/add_shipping_address_view.dart';
import 'package:ecommerce/presentation/features/checkout/views/card_view.dart';
import 'package:ecommerce/presentation/features/checkout/views/payment_method_view.dart';
import 'package:ecommerce/presentation/features/checkout/views/shipping_address_view.dart';
import 'package:ecommerce/presentation/features/checkout/views/success1_view.dart';
import 'package:ecommerce/presentation/features/checkout/views/success2_view.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CheckoutScreen extends StatefulWidget {
  const CheckoutScreen({Key? key}) : super(key: key);

  @override
  State<CheckoutScreen> createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: OpenFlutterScaffold(
          background: null,
          title: 'Checkout',
          body: BlocProvider<CheckoutBloc>(
              create: (context) {
                return CheckoutBloc()
                  ..add(CheckoutStartEvent());
              },
              child: const CheckoutWrapper()),
          bottomMenuIndex: 2,
        ));
  }
}

class CheckoutWrapper extends StatefulWidget {
  const CheckoutWrapper({Key? key}) : super(key: key);

  @override
  _CheckoutWrapperState createState() => _CheckoutWrapperState();
}

class _CheckoutWrapperState extends OpenFlutterWrapperState<CheckoutWrapper> {
  //State createState() => OpenFlutterWrapperState();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CheckoutBloc, CheckoutState>(
        builder: (BuildContext context, CheckoutState state) {
          return getPageView(<Widget>[
            CartView(changeView: changePage),
            PaymentMethodView(changeView: changePage),
            ShippingAddressView(changeView: changePage),
            AddShippingAddressView(changeView: changePage),
            Success1View(changeView: changePage),
            Success2View(changeView: changePage),
          ]);
        });
  }
}