import 'package:equatable/equatable.dart';

abstract class CheckoutEvent extends Equatable {
  @override
  List<Object?> get props => [];

  @override
  bool? get stringify => true;
}

class CheckoutStartEvent extends CheckoutEvent {}

class CheckoutFinishEvent extends CheckoutEvent {}

class CheckoutShowAddNewCardEvent extends CheckoutEvent {}

class CheckoutSetDefaultCardEvent extends CheckoutEvent {
  final int cardId;

  CheckoutSetDefaultCardEvent(this.cardId);

  @override
  List<Object?> get props => [cardId];
}

class CheckoutAddNewCardEvent extends CheckoutEvent {
  final String nameOnCard;
  final String cardNumber;
  final int expirationMonth;
  final int expirationYear;
  final int cvv;
  final bool? setAsDefault;

  CheckoutAddNewCardEvent({
    required this.nameOnCard,
    required this.cardNumber,
    required this.expirationMonth,
    required this.expirationYear,
    required this.cvv,
    this.setAsDefault,
  });

  @override
  List<Object?> get props => [
        nameOnCard,
        cardNumber,
        expirationMonth,
        expirationYear,
        cvv,
        setAsDefault
      ];
}

class CheckoutSetDefaultShippingAddressEvent extends CheckoutEvent {
  final int shippingAddressId;

  CheckoutSetDefaultShippingAddressEvent(this.shippingAddressId);
}

class CheckoutAddNewShippingAddressEvent extends CheckoutEvent {
  final String fullName;
  final String address;
  final String city;
  final String state;
  final String postal;
  final String country;

  CheckoutAddNewShippingAddressEvent(
      {required this.fullName,
      required this.address,
      required this.city,
      required this.state,
      required this.postal,
      required this.country});
}
