import 'package:bloc/bloc.dart';
import 'package:ecommerce/domain/usecases/checkout/checkout_start_use_case.dart';
import 'package:ecommerce/locator.dart';

import 'checkout_event.dart';
import 'checkout_state.dart';

class CheckoutBloc extends Bloc<CheckoutEvent, CheckoutState> {
  final CheckoutStartUseCase _checkoutStartUseCase;

  CheckoutBloc()
      : _checkoutStartUseCase = sl(),
        super(CheckoutInitialState()) {
    on<CheckoutAddNewCardEvent>(_onNewCardAdded);
    on<CheckoutShowAddNewCardEvent>(_onNewCardShowed);
    on<CheckoutSetDefaultCardEvent>(_onDefaultCardSet);
    on<CheckoutStartEvent>(_onCheckoutStarted);
  }

  Future<void> _onCheckoutStarted(
    CheckoutStartEvent event,
    Emitter<CheckoutState> emit,
  ) async {
    if (state is CheckoutInitialState) {
      CheckoutStartResult results = await _checkoutStartUseCase.execute(CheckoutStartParams());
      emit(CheckoutProceedState(cardId: 1, cartProducts: results.cartItems,
          shippingAddresses: results.shippingAddress,
          paymentMethods: results.paymentMethods,
          currentPaymentMethod: results.currentPaymentMethod,
          currentShippingAddress: results.currentShippingAddress,
          orderPrice: results.totalCalculatedPrice ?? 0,
          deliveryPrice: results.deliveryPrice ?? 0,
          summaryPrice: results.summaryPrice ?? 0));
    } else {
      emit(state);
    }
  }

  Future<void> _onDefaultCardSet(
    CheckoutSetDefaultCardEvent event,
    Emitter<CheckoutState> emit,
  ) async {
    if (state is CheckoutProceedState) {
      emit((state as CheckoutProceedState).copyWith(cardId: event.cardId));
    }
  }

  Future<void> _onNewCardShowed(
    CheckoutShowAddNewCardEvent event,
    Emitter<CheckoutState> emit,
  ) async {
    if (state is CheckoutProceedState) {
      emit((state as CheckoutProceedState).copyWith(showAddNewCardForm: true));
    }
  }

  Future<void> _onNewCardAdded(
    CheckoutAddNewCardEvent event,
    Emitter<CheckoutState> emit,
  ) async {
    if (state is CheckoutProceedState) {
      emit((state as CheckoutProceedState).copyWith(showAddNewCardForm: false));
    }
  }
}
