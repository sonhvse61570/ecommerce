import 'package:ecommerce/data/model/category.dart';
import 'package:equatable/equatable.dart';

class CategoriesState extends Equatable {
  final int parentCategoryId;

  const CategoriesState({this.parentCategoryId = 0});

  @override
  List<Object?> get props => [parentCategoryId];
}

class CategoriesLoadingState extends CategoriesState {
  @override
  String toString() => 'CategoriesInitialState';
}

class CategoriesViewState extends CategoriesState {
  final List<ProductCategory> categories;

  const CategoriesViewState(
      {required int parentCategoryId, required this.categories})
      : super(parentCategoryId: parentCategoryId);

  @override
  List<Object?> get props => [categories, parentCategoryId];
}

class CategoriesListViewState extends CategoriesViewState {
  const CategoriesListViewState(
      {required int parentCategoryId,
      required List<ProductCategory> categories})
      : super(parentCategoryId: parentCategoryId, categories: categories);

  @override
  String toString() => 'CategoriesListViewState';
}

class CategoriesTileViewState extends CategoriesViewState {
  const CategoriesTileViewState({
    required int parentCategoryId,
    required List<ProductCategory> categories,
  }) : super(parentCategoryId: parentCategoryId, categories: categories);

  @override
  String toString() => 'CategoriesTileViewState';
}

class CategoriesErrorState extends CategoriesState {
  @override
  String toString() => 'CategoriesErrorState';
}
