import 'package:ecommerce/config/app_routes.dart';
import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/category.dart';
import 'package:ecommerce/presentation/features/categories/categories_bloc.dart';
import 'package:ecommerce/presentation/features/categories/categories_event.dart';
import 'package:ecommerce/presentation/features/categories/categories_state.dart';
import 'package:ecommerce/presentation/features/products/products_screen.dart';
import 'package:ecommerce/presentation/widgets/data_driven/category_list_element.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CategoriesListView extends StatefulWidget {
  const CategoriesListView({Key? key}) : super(key: key);

  @override
  State<CategoriesListView> createState() => _CategoriesListViewState();
}

class _CategoriesListViewState extends State<CategoriesListView> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var widgetWidth = width - AppSizes.sidePadding * 4;
    var _theme = Theme.of(context);
    return BlocListener<CategoriesBloc, CategoriesState>(
        listener: (context, state) {
          if (state is CategoriesErrorState) {
            Container(
                padding: const EdgeInsets.all(AppSizes.sidePadding),
                child: Text('An error occur',
                    style: _theme.textTheme.headline4
                        ?.copyWith(color: _theme.errorColor)));
          }
        }, child:
    BlocBuilder<CategoriesBloc, CategoriesState>(builder: (context, state) {
      if (state is CategoriesListViewState) {
        return SingleChildScrollView(
          child: Column(
            children: <Widget>[
              const Padding(padding: EdgeInsets.only(top: AppSizes.sidePadding)),
              OpenFlutterButton(
                onPressed: (() => {
                  BlocProvider.of<CategoriesBloc>(context)
                      .add(const CategoriesShowTilesEvent(0)),
                }),
                title: 'VIEW ALL ITEMS',
                width: widgetWidth,
                height: 50.0,
              ),
              const Padding(
                padding: EdgeInsets.only(
                  top: AppSizes.sidePadding,
                ),
              ),
              Column(
                children: buildCategoryList(state.categories),
              )
            ],
          ),
        );
      }
      return const Center(
        child: CircularProgressIndicator(),
      );
    }));
  }

  List<Widget> buildCategoryList(List<ProductCategory>? categories) {
    var elements = <Widget>[];
    if (categories != null) {
      for (var i = 0; i < categories.length; i++) {
        elements.add(
          InkWell(
            onTap: categories[i].isCategoryContainer
                ? () {
              BlocProvider.of<CategoriesBloc>(context)
                  .add(ChangeCategoriesParent(categories[i].id));
            }
                : () {
              Navigator.of(context).pushNamed(
                  AppRoutes.productList,
                  arguments: ProductListScreenParameters(categories[i]));
            },
            child: OpenFlutterCategoryListElement(category: categories[i]),
          ),
        );
      }
    }
    return elements;
  }
}
