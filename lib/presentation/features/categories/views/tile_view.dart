import 'package:ecommerce/config/app_routes.dart';
import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/category.dart';
import 'package:ecommerce/presentation/features/categories/categories_bloc.dart';
import 'package:ecommerce/presentation/features/categories/categories_state.dart';
import 'package:ecommerce/presentation/features/products/products_screen.dart';
import 'package:ecommerce/presentation/widgets/data_driven/category_tile.dart';
import 'package:ecommerce/presentation/widgets/independent/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CategoriesTileView extends StatefulWidget {
  final Function? changeView;

  const CategoriesTileView({Key? key, this.changeView}) : super(key: key);

  @override
  State<CategoriesTileView> createState() => _CategoriesTileViewState();
}

class _CategoriesTileViewState extends State<CategoriesTileView>
    with SingleTickerProviderStateMixin {
  final List<String> types = ["Women", "Men", "Kids"];
  final List<int> categoryIds = [1, 2, 3];
  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: types.length, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);
    var width = MediaQuery.of(context).size.width;

    return BlocListener<CategoriesBloc, CategoriesState>(
        listenWhen: (context, state) {
          return state is CategoriesErrorState;
        }, listener: (BuildContext context, CategoriesState state) {
      Container(
          padding: const EdgeInsets.all(AppSizes.sidePadding),
          child: Text('An error occur',
              style: _theme.textTheme.headline4
                  ?.copyWith(color: _theme.errorColor)));
    }, child:
    BlocBuilder<CategoriesBloc, CategoriesState>(builder: (context, state) {
      if (state is CategoriesTileViewState) {
        var tabViews = <Widget>[];
        for (var _ in categoryIds) {
          tabViews.add(SingleChildScrollView(
              child: Column(children: <Widget>[
                Padding(
                    padding: const EdgeInsets.all(AppSizes.sidePadding),
                    child: Container(
                        width: width,
                        padding: const EdgeInsets.all(AppSizes.sidePadding * 2),
                        decoration: BoxDecoration(
                          color: _theme.colorScheme.secondary,
                          borderRadius: BorderRadius.circular(AppSizes.imageRadius),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text('SUMMER SALES',
                                style: _theme.textTheme.headline4
                                    ?.copyWith(color: AppColors.white)),
                            Text('Up to 50% off',
                                style: _theme.textTheme.headline4
                                    ?.copyWith(color: AppColors.white))
                          ],
                        ))),
                Container(
                    padding: const EdgeInsets.all(AppSizes.sidePadding),
                    child: Column(
                        children: buildCategoryList(
                            state.categories, width - AppSizes.sidePadding * 3)))
              ])));
        }
        return SafeArea(
            child: OpenFlutterScaffold(
                background: null,
                title: 'Categories',
                bottomMenuIndex: 1,
                tabController: _tabController,
                tabBarList: types,
                body: TabBarView(
                  children: tabViews,
                  controller: _tabController,
                )));
      }
      return const Center(child: CircularProgressIndicator());
    }));
  }

  List<Widget> buildCategoryList(
      List<ProductCategory> categories, double width) {
    var elements = <Widget>[];
    for (var i = 0; i < categories.length; i++) {
      elements.add(InkWell(
          onTap: (() {
            Navigator.of(context).pushNamed(
                AppRoutes.productList,
                arguments: ProductListScreenParameters(categories[i]));
          }),
          child: OpenFlutterCategoryTile(
              height: 100, width: width, category: categories[i])));
    }
    return elements;
  }
}
