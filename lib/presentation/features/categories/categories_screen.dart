
import 'package:ecommerce/presentation/features/categories/categories_bloc.dart';
import 'package:ecommerce/presentation/features/categories/categories_state.dart';
import 'package:ecommerce/presentation/features/categories/views/list_view.dart';
import 'package:ecommerce/presentation/features/categories/views/tile_view.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/loading_view.dart';
import 'package:ecommerce/presentation/widgets/independent/scaffold.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CategoriesScreen extends StatefulWidget {
  const CategoriesScreen({Key? key}) : super(key: key);

  @override
  State<CategoriesScreen> createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)?.settings.arguments as CategoriesParameters?;

    if (kDebugMode) {
      print('widget parameters at categories screen $args');
    }

    return Container();
  }
}

class CategoriesWrapper extends StatefulWidget {
  const CategoriesWrapper({Key? key}) : super(key: key);

  @override
  _CategoriesWrapperState createState() => _CategoriesWrapperState();
}

class _CategoriesWrapperState
    extends OpenFlutterWrapperState<CategoriesWrapper> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<CategoriesBloc, CategoriesState>(
        child: getPageView(<Widget>[
          LoadingView(),
          buildListScreen(context),
          CategoriesTileView(changeView: changePage),
        ]),
        listener: (BuildContext context, CategoriesState state) {
          final index = state is CategoriesLoadingState
              ? 0
              : state is CategoriesListViewState ? 1 : 2;
          changePage(changeType: ViewChangeType.Exact, index: index);
        });
  }

  Widget buildListScreen(BuildContext context) {
    return const OpenFlutterScaffold(
      background: null,
      title: 'Categories',
      body: CategoriesListView(),
      bottomMenuIndex: 1,
    );
  }
}

class CategoriesParameters {
  final int categoryId;

  const CategoriesParameters(this.categoryId);
}