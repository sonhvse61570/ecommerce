import 'package:bloc/bloc.dart';
import 'package:ecommerce/data/model/category.dart';
import 'package:ecommerce/domain/usecases/categories/find_categories_by_filter_use_case.dart';
import 'package:ecommerce/locator.dart';

import 'categories_event.dart';
import 'categories_state.dart';

class CategoriesBloc extends Bloc<CategoriesEvent, CategoriesState> {
  final FindCategoriesByFilterUseCase _findCategoriesByFilterUseCase;

  CategoriesBloc()
      : _findCategoriesByFilterUseCase = sl(),
        super(CategoriesLoadingState()) {
    on<CategoriesShowListEvent>(_onCategoriesFetched);
    on<CategoriesShowTilesEvent>(_onCategoryShowTilesFetched);
    on<ChangeCategoriesParent>(_onCategoryParentChanged);
  }

  Future<void> _onCategoriesFetched(
    CategoriesShowListEvent event,
    Emitter<CategoriesState> emit,
  ) async {
    if (state is CategoriesListViewState) {
      if (state.parentCategoryId != event.parentCategoryId) {
        emit(CategoriesLoadingState());
        List<ProductCategory> categories =
            await _getCategoriesByFilter(event.parentCategoryId);
        emit(CategoriesListViewState(
            parentCategoryId: event.parentCategoryId, categories: categories));
      }
    } else {
      emit(CategoriesLoadingState());
      List<ProductCategory> categories =
          await _getCategoriesByFilter(event.parentCategoryId);
      emit(CategoriesListViewState(
          parentCategoryId: event.parentCategoryId, categories: categories));
    }
  }

  Future<void> _onCategoryShowTilesFetched(
    CategoriesShowTilesEvent event,
    Emitter<CategoriesState> emit,
  ) async {
    if (state is CategoriesTileViewState) {
      if (state.parentCategoryId != event.parentCategoryId) {
        emit(CategoriesLoadingState());
        List<ProductCategory> categories =
            await _getCategoriesByFilter(event.parentCategoryId);
        emit(CategoriesTileViewState(
            parentCategoryId: event.parentCategoryId, categories: categories));
      }
    } else {
      emit(CategoriesLoadingState());
      List<ProductCategory> categories =
          await _getCategoriesByFilter(event.parentCategoryId);
      emit(CategoriesTileViewState(
          parentCategoryId: event.parentCategoryId, categories: categories));
    }
  }

  Future<void> _onCategoryParentChanged(
    ChangeCategoriesParent event,
    Emitter<CategoriesState> emit,
  ) async {
    emit(CategoriesLoadingState());

    List<ProductCategory> categories =
        await _getCategoriesByFilter(event.parentCategoryId);
    if (state is CategoriesTileViewState) {
      emit(CategoriesTileViewState(
          parentCategoryId: event.parentCategoryId, categories: categories));
    } else {
      emit(CategoriesListViewState(
          parentCategoryId: event.parentCategoryId, categories: categories));
    }
  }

  Future<List<ProductCategory>> _getCategoriesByFilter(int categoryId) async {
    final categoriesData = await _findCategoriesByFilterUseCase
        .execute(CategoriesByFilterParams(categoryId: categoryId));
    return categoriesData.categories;
  }
}
