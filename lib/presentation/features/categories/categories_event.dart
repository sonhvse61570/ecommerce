import 'package:equatable/equatable.dart';

abstract class CategoriesEvent extends Equatable {
  final int parentCategoryId;

  const CategoriesEvent(this.parentCategoryId) : super();

  @override
  List<Object?> get props => [parentCategoryId];
}

class CategoriesShowListEvent extends CategoriesEvent {
  const CategoriesShowListEvent(int parentCategoryId) : super(parentCategoryId);
}

class CategoriesShowTilesEvent extends CategoriesEvent {
  const CategoriesShowTilesEvent(int parentCategoryId)
      : super(parentCategoryId);
}

class ChangeCategoriesParent extends CategoriesEvent {
  const ChangeCategoriesParent(int parentCategoryId) : super(parentCategoryId);
}
