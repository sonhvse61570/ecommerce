import 'package:equatable/equatable.dart';

abstract class RecoverPasswordEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class RecoverPasswordPressed extends RecoverPasswordEvent {
  final String email;

  RecoverPasswordPressed(this.email);

  @override
  List<Object?> get props => [email];
}
