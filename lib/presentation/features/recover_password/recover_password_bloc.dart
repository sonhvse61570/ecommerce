import 'package:bloc/bloc.dart';
import 'package:ecommerce/data/repositories/abstract/user_repository.dart';

import 'recover_password_event.dart';
import 'recover_password_state.dart';

class RecoverPasswordBloc
    extends Bloc<RecoverPasswordEvent, RecoverPasswordState> {
  final UserRepository _userRepository;

  RecoverPasswordBloc({required UserRepository userRepository})
      : _userRepository = userRepository,
        super(RecoverPasswordInitialState()) {
    on<RecoverPasswordPressed>(_onRecoverPressed);
  }

  Future<void> _onRecoverPressed(
    RecoverPasswordPressed event,
    Emitter<RecoverPasswordState> emit,
  ) async {
    emit(RecoverPasswordProcessingState());

    try {
      await _userRepository.forgotPassword(email: event.email);
    } catch (e) {
      emit(RecoverPasswordErrorState(e.toString()));
    }
  }
}
