import 'package:equatable/equatable.dart';

abstract class RecoverPasswordState extends Equatable {
  @override
  List<Object?> get props => [];
}

class RecoverPasswordInitialState extends RecoverPasswordState {}

class RecoverPasswordProcessingState extends RecoverPasswordState {}

class RecoverPasswordErrorState extends RecoverPasswordState {
  final String error;

  RecoverPasswordErrorState(this.error);

  @override
  List<Object?> get props => [error];
}

class RecoverPasswordFinishedState extends RecoverPasswordState {
  final String email;

  RecoverPasswordFinishedState(this.email);

  @override
  List<Object?> get props => [email];
}