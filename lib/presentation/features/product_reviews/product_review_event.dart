import 'package:equatable/equatable.dart';

class ProductReviewEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class ProductReviewStartEvent extends ProductReviewEvent {
  final int productId;

  final bool withPhotos;

  ProductReviewStartEvent(this.productId, this.withPhotos);
}