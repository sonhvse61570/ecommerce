import 'package:bloc/bloc.dart';
import 'package:ecommerce/data/model/product_review.dart';
import 'package:ecommerce/data/repositories/mock/product_review_mock_repository.dart';

import 'product_review_event.dart';
import 'product_review_state.dart';

class ProductReviewBloc extends Bloc<ProductReviewEvent, ProductReviewState> {
  final ProductReviewMockRepository _productReviewMockRepository;

  ProductReviewBloc(
      {required ProductReviewMockRepository productReviewMockRepository})
      : _productReviewMockRepository = productReviewMockRepository,
        super(ProductReviewInitialState()) {
    on<ProductReviewStartEvent>(_onCommentReviewsFetched);
  }

  Future<void> _onCommentReviewsFetched(
    ProductReviewStartEvent event,
    Emitter<ProductReviewState> emit,
  ) async {
    emit(ProductReviewLoadingState());

    var comments = await _getCommentsReviews(event.productId, event.withPhotos);
    var data = ProductReviewData(comments, comments.length, event.withPhotos);
    emit(ProductReviewLoadedState(data));
  }

  Future<List<ProductReview>> _getCommentsReviews(
      int productId, bool withPhotos) async {
    return await _productReviewMockRepository.findReviewsByProductId(
        productId, withPhotos);
  }
}
