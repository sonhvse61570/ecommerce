import 'package:ecommerce/data/model/product_review.dart';
import 'package:equatable/equatable.dart';

class ProductReviewData {
  final List<ProductReview> reviews;
  final int reviewCounter;
  final bool withPhotos;

  ProductReviewData(this.reviews, this.reviewCounter, this.withPhotos);
}

class ProductReviewState extends Equatable {
  @override
  List<Object?> get props => [];
}

class ProductReviewInitialState extends ProductReviewState {}

class ProductReviewLoadedState extends ProductReviewState {
  final ProductReviewData data;

  ProductReviewLoadedState(this.data);
}

class ProductReviewLoadingState extends ProductReviewState {}

class ProductReviewWithPhotosState extends ProductReviewLoadedState {
  final bool withPhotos;

  ProductReviewWithPhotosState(ProductReviewData data, this.withPhotos) : super(data);
}