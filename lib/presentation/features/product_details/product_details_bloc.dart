import 'dart:collection';

import 'package:bloc/bloc.dart';
import 'package:ecommerce/data/model/cart_item.dart';
import 'package:ecommerce/data/model/favorite_product.dart';
import 'package:ecommerce/data/model/product_attribute.dart';
import 'package:ecommerce/domain/usecases/cart/add_product_to_cart_use_case.dart';
import 'package:ecommerce/domain/usecases/favorites/add_to_favorites_use_case.dart';
import 'package:ecommerce/domain/usecases/favorites/remove_from_favorites_use_case.dart';
import 'package:ecommerce/domain/usecases/products/get_product_by_id_use_case.dart';
import 'package:ecommerce/locator.dart';

import 'product_details_event.dart';
import 'product_details_state.dart';

class ProductDetailsBloc
    extends Bloc<ProductDetailsEvent, ProductDetailsState> {
  final AddToFavoritesUseCase addToFavoritesUseCase;
  final RemoveFromFavoritesUseCase removeFromFavoritesUseCase;
  final GetProductByIdUseCase getProductByIdUseCase;
  final AddProductToCartUseCase addProductToCartUseCase;

  final int? productId;

  ProductDetailsBloc(this.productId)
      : getProductByIdUseCase = sl(),
        addProductToCartUseCase = sl(),
        addToFavoritesUseCase = sl(),
        removeFromFavoritesUseCase = sl(),
        super(ProductDetailsInitialState()) {
    on<ProductDetailsLoadedEvent>(_onDetailFetched);
    on<ProductAddToFavoritesEvent>(_onFavoritesAdded);
    on<ProductRemoveFromFavoritesEvent>(_onFavoritesRemoved);
    on<ProductAddToCartEvent>(_onCartAdded);
    on<ProductSetAttributeEvent>(_onProductAttributeSet);
  }

  Future<void> _onDetailFetched(
    ProductDetailsLoadedEvent event,
    Emitter<ProductDetailsState> emit,
  ) async {
    emit(ProductDetailsLoadingState());
    final ProductDetailsResults data =  await getProductByIdUseCase.execute(
        ProductDetailsParams(
            categoryId: event.categoryId,
            productId: event.productId)
    );
    emit(ProductDetailsLoadedState(product: data.productDetails,
        similarProducts: data.similarProducts,
        productAttributes: SelectedProductAttributes(
          selectedAttributes: HashMap<ProductAttribute, String>(),
        )
    ));
  }

  Future<void> _onFavoritesAdded(
    ProductAddToFavoritesEvent event,
    Emitter<ProductDetailsState> emit,
  ) async {
    ProductDetailsLoadedState currentState = state as ProductDetailsLoadedState;
    await addToFavoritesUseCase.execute(
        FavoriteProduct(
            currentState.product!,
            currentState.productAttributes.selectedAttributes
        )
    );
  }

  Future<void> _onFavoritesRemoved(
    ProductRemoveFromFavoritesEvent event,
    Emitter<ProductDetailsState> emit,
  ) async {
    if ( state is ProductDetailsLoadedState) {
      ProductDetailsLoadedState currentState = state as ProductDetailsLoadedState;
      await removeFromFavoritesUseCase.execute(
          RemoveFromFavoritesParams(
              FavoriteProduct(
                  currentState.product!,
                  currentState.productAttributes.selectedAttributes
              )
          )
      );
    }
  }

  Future<void> _onCartAdded(
    ProductAddToCartEvent event,
    Emitter<ProductDetailsState> emit,
  ) async {
    if ( state is ProductDetailsLoadedState) {
      ProductDetailsLoadedState currentState = state as ProductDetailsLoadedState;
      AddToCartResult addToCartResult = await addProductToCartUseCase.execute(CartItem(
          product: currentState.product!,
          productQuantity: ProductQuantity(1),
          selectedAttributes: currentState.productAttributes.selectedAttributes
      ));
      if ( !addToCartResult.result) {
        //TODO: show SnackBar with error
      }
    }
  }

  Future<void> _onProductAttributeSet(
    ProductSetAttributeEvent event,
    Emitter<ProductDetailsState> emit,
  ) async {
    ProductDetailsLoadedState newState = state as ProductDetailsLoadedState;
    emit(ProductDetailsLoadingState());
    newState.productAttributes.selectedAttributes[event.attribute] =event.selectedValue;
    emit(ProductDetailsLoadedState(product: newState.product,
        similarProducts: newState.similarProducts,
        productAttributes: newState.productAttributes
    ));
  }
}
