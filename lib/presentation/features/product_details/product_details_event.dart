import 'package:ecommerce/data/model/product_attribute.dart';
import 'package:equatable/equatable.dart';

class ProductDetailsEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class ProductDetailsLoadedEvent extends ProductDetailsEvent {
  final int productId;
  final int categoryId;

  ProductDetailsLoadedEvent({
    required this.productId,
    required this.categoryId,
  });
}

class ProductAddToFavoritesEvent extends ProductDetailsEvent {}

class ProductRemoveFromFavoritesEvent extends ProductDetailsEvent {}

class ProductAddToCartEvent extends ProductDetailsEvent {}

class ProductSetAttributeEvent extends ProductDetailsEvent {
  final String selectedValue;
  final ProductAttribute attribute;

  ProductSetAttributeEvent(this.selectedValue, this.attribute);
}