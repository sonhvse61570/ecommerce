import 'dart:collection';

import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/model/product_attribute.dart';
import 'package:equatable/equatable.dart';

class ProductDetailsState extends Equatable {
  @override
  List<Object?> get props => [];
}

class ProductDetailsInitialState extends ProductDetailsState {}

class ProductDetailsLoadedState extends ProductDetailsState {
  final Product? product;
  final List<Product> similarProducts;
  final SelectedProductAttributes productAttributes;

  ProductDetailsLoadedState({
    required this.product,
    required this.similarProducts,
    required this.productAttributes,
  });

  ProductDetailsLoadedState copyWith(
    Product? product,
    List<Product> similarProducts,
    Map<ProductAttribute, String> productAttrs,
  ) =>
      ProductDetailsLoadedState(
          product: product ?? this.product,
          similarProducts: similarProducts,
          productAttributes: productAttributes);
}

class SelectedProductAttributes extends Equatable {
  final HashMap<ProductAttribute, String> selectedAttributes;

  const SelectedProductAttributes({required this.selectedAttributes});

  @override
  List<Object?> get props => [selectedAttributes, selectedAttributes.values];
}

class ProductDetailsLoadingState extends ProductDetailsState {}

class ProductErrorState extends ProductDetailsState {}
