import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/product_attribute.dart';
import 'package:flutter/material.dart';

class AttributeBottomSheet extends StatelessWidget {
  final ProductAttribute productAttribute;
  final String selectedValue;
  final Function(String, ProductAttribute) onValueSelect;

  const AttributeBottomSheet(
      {Key? key,
      required this.productAttribute,
      required this.selectedValue,
      required this.onValueSelect})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      heightFactor: 0.12 + productAttribute.options.length / 3 * 0.1,
      child: Container(
        padding: AppSizes.bottomSheetPadding,
        decoration: const BoxDecoration(
            color: AppColors.background,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(34), topRight: Radius.circular(34)),
            boxShadow: []),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: 6,
                width: 60,
                decoration: BoxDecoration(
                  color: AppColors.lightGray,
                  borderRadius: BorderRadius.circular(3),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                'Select ${productAttribute.name}',
                style: const TextStyle(
                    color: AppColors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 18,
              ),
              GridView.count(
                shrinkWrap: true,
                crossAxisCount: 3,
                childAspectRatio: 100 / 60,
                children: productAttribute.options
                    .map((String value) => InkWell(
                  onTap: () =>
                  {onValueSelect(value, productAttribute)},
                  child: Container(
                    margin: const EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: selectedValue == value
                                ? AppColors.red
                                : AppColors.darkGray),
                        borderRadius:
                        const BorderRadius.all(Radius.circular(15.0))),
                    child: Center(
                      child: Text(
                        value,
                        style: const TextStyle(fontSize: 15.0),
                      ),
                    ),
                  ),
                ))
                    .toList(),
              ),
              const SizedBox(
                height: 10.0,
              ),
              productAttribute.info == null
                  ? Container()
                  : Theme(
                  data: Theme.of(context)
                      .copyWith(dividerColor: AppColors.darkGray),
                  child: const Divider()),
              productAttribute.info == null
                  ? Container()
                  : ExpansionTile(
                title: Text('${productAttribute.name} info'),
                trailing: const Icon(Icons.keyboard_arrow_right),
                //TODO show info on click
              ),
              Theme(
                  data: Theme.of(context)
                      .copyWith(dividerColor: AppColors.darkGray),
                  child: const Divider()),
              const SizedBox(
                height: 10.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
