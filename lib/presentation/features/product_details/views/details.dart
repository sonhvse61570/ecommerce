import 'package:ecommerce/config/app_routes.dart';
import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/category.dart';
import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/model/product_attribute.dart';
import 'package:ecommerce/presentation/features/home/home_bloc.dart';
import 'package:ecommerce/presentation/features/home/home_event.dart';
import 'package:ecommerce/presentation/features/product_details/product_details_bloc.dart';
import 'package:ecommerce/presentation/features/product_details/product_details_event.dart';
import 'package:ecommerce/presentation/features/product_details/product_details_state.dart';
import 'package:ecommerce/presentation/features/product_reviews/product_review_and_rating_screen.dart';
import 'package:ecommerce/presentation/widgets/data_driven/product_list_view.dart';
import 'package:ecommerce/presentation/widgets/independent/custom_button.dart';
import 'package:ecommerce/presentation/widgets/independent/expansion_tile.dart';
import 'package:ecommerce/presentation/widgets/independent/favourite_button.dart';
import 'package:ecommerce/presentation/widgets/independent/product_rating.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'attribute_bottom_sheet.dart';

class ProductDetailsView extends StatefulWidget {
  final Product product;
  final Function changeView;
  final ProductCategory? category;
  final bool hasReviews;

  final List<Product> similarProducts;

  const ProductDetailsView(
      {Key? key,
      required this.product,
      required this.changeView,
      this.category,
      this.hasReviews = false,
      required this.similarProducts})
      : super(key: key);

  @override
  State<ProductDetailsView> createState() => _ProductDetailsViewState();
}

class _ProductDetailsViewState extends State<ProductDetailsView> {

  Orientation? orientation;
  bool? favorite;
  ProductDetailsBloc? bloc;

  @override
  void initState() {
    favorite = widget.product.isFavorite;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _theme = Theme.of(context);
    final dividerTheme =
    Theme.of(context).copyWith(dividerColor: AppColors.darkGray);
    var deviceWidth = MediaQuery.of(context).size.width;
    var deviceHeight = MediaQuery.of(context).size.height;
    bloc = BlocProvider.of<ProductDetailsBloc>(context);

    return BlocListener(
        bloc: bloc,
        listener: (context, state) {
          if (state is ProductErrorState) {
            Container(
                padding: const EdgeInsets.all(AppSizes.sidePadding),
                child: Text('An error occur',
                    style: _theme.textTheme.headline4
                        ?.copyWith(color: _theme.errorColor)));
          }
        },
        child: BlocBuilder(
            bloc: bloc,
            builder: (BuildContext context, ProductDetailsState state) {
              if (state is ProductDetailsLoadedState) {
                return SingleChildScrollView(
                    child: Container(
                      color: Colors.white,
                      child: Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              height: deviceHeight * 0.52,
                              child: ListView.builder(
                                itemBuilder: (context, index) => Padding(
                                    padding: const EdgeInsets.only(
                                        right: AppSizes.sidePadding),
                                    child: Image.network(
                                        state.product!.images[index].address)),
                                scrollDirection: Axis.horizontal,
                                itemCount: state.product?.images.length ?? 0,
                              ),
                            ),
                            Container(
                              //contains size button,color button and favourite button
                              margin: const EdgeInsets.all(16),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  mainAxisSize: MainAxisSize.max,
                                  children: (state.product?.selectableAttributes !=
                                      null
                                      ? state.product?.selectableAttributes
                                      .map((value) =>
                                      selectionOutlineButton(
                                          deviceWidth,
                                          value,
                                          state.productAttributes
                                              .selectedAttributes[
                                          value] ?? ""))
                                      .toList()
                                      : <Widget>[])! +
                                      [
                                        OpenFlutterFavouriteButton(
                                          favourite: favorite,
                                          setFavourite: () => {setFavourite(bloc!)},
                                        )
                                      ]),
                            ),
                            productDetails(_theme),
                            //Function call for Product detail widget
                            Container(
                              margin: const EdgeInsets.only(
                                  left: 16.0, right: 16.0, bottom: 10.0),
                              child: Text(
                                widget.product.description,
                                style: const TextStyle(fontSize: 15.0),
                              ),
                            ),
                            Container(
                              width: deviceWidth,
                              margin: const EdgeInsets.only(bottom: 13),
                              height: 90,
                              child: Center(
                                child: OpenFlutterButton(
                                  title: 'ADD TO CART',
                                  onPressed: () {
                                    _addItemToCart(context, state);
                                  },
                                  width: deviceWidth * 0.88,
                                  height: 50,
                                ),
                              ),
                            ),
                            Theme(data: dividerTheme, child: const Divider()),
                            OpenFlutterExpansionTile(
                              title: 'Shipping info',
                              //TODO: get this date from store settings
                              description: 'detailed data about shipping options',
                            ),
                            Theme(data: dividerTheme, child: const Divider()),
                            OpenFlutterExpansionTile(
                              title: 'Support',
                              //TODO: get this date from store settings
                              description: 'detailed data about support',
                            ),
                            Theme(data: dividerTheme, child: const Divider()),
                            const SizedBox(
                              height: 10.0,
                            ),
                            Container(
                              margin: const EdgeInsets.only(
                                  left: 16.0, right: 16.0, bottom: 12.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  const Text(
                                    'You can also like this',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    widget.similarProducts.length.toString() +
                                        ' items',
                                    style: const TextStyle(color: AppColors.lightGray),
                                  )
                                ],
                              ),
                            ),
                            OpenFlutterProductListView(
                              width: deviceWidth,
                              products: widget.similarProducts,
                              onFavoritesTap: ((Product product) => {
                                BlocProvider.of<HomeBloc>(context).add(
                                    HomeAddToFavoriteEvent(
                                        !product.isFavorite,
                                        product))
                              }),
                            )
                          ],
                        ),
                      ),
                    ));
              }
              return Container();
            }));
  }

  void setFavourite(ProductDetailsBloc bloc) {
    if (!favorite!) {
      bloc.add(
          ProductAddToFavoritesEvent()); //TODO ask for real parameters if required
    } else {
      bloc.add(ProductRemoveFromFavoritesEvent());
    }
    setState(() {
      favorite = !favorite!;
    });
  }

  void _showSelectAttributeBottomSheet(
      BuildContext context, ProductAttribute attribute,
      {Function? onSelect, String? selectedValue}) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(34), topRight: Radius.circular(34)),
        ),
        builder: (BuildContext context) => AttributeBottomSheet(
            productAttribute: attribute,
            selectedValue: selectedValue ?? "",
            onValueSelect: ((String value, ProductAttribute productAttribute) =>
            {
              bloc!..add(ProductSetAttributeEvent(value, productAttribute)),
              Navigator.pop(context),
              onSelect?.call()
            })));
  } //modelBottomSheet for selecting size

  Widget selectionOutlineButton(var deviceWidth, ProductAttribute attribute,
      String alreadySelectedValue) {
    //select size and select color widget
    return MaterialButton(
      onPressed: () => _showSelectAttributeBottomSheet(context, attribute,
          selectedValue: alreadySelectedValue),
      child: Container(
        margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              alreadySelectedValue,
              style: const TextStyle(
                  fontSize: 14,
                  color: AppColors.black,
                  fontWeight: FontWeight.w300),
            ),
            const Icon(Icons.keyboard_arrow_down)
          ],
        ),
        width: deviceWidth * 0.29,
      ),
      /*borderSide: BorderSide(color: AppColors.darkGray),
      highlightedBorderColor: AppColors.red,*/
      focusColor: AppColors.white,
      highlightColor: Colors.white,
      hoverColor: AppColors.red,
      shape:
      ContinuousRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
    );
  }

  Widget productDetails(ThemeData theme) {
    //title,rating,price of product
    return Container(
      margin: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 17.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                widget.product.title,
                style: theme.textTheme.headline6,
              ),
              Text(
                '\$' + widget.product.price.toString(),
                style: theme.textTheme.headline6,
              )
            ],
          ),
          widget.category == null
              ? Container()
              : Text(
            widget.category!.name,
            style: theme.textTheme.bodyText1,
          ),
          const SizedBox(
            height: 5,
          ),
          GestureDetector(
            onTap: widget.hasReviews
                ? () {
              navigateToReviewDetail(context);
            }
                : null,
            child: OpenFlutterProductRating(
              rating: widget.product.averageRating,
              ratingCount: widget.product.ratingCount,
              alignment: MainAxisAlignment.start,
            ),
          ),
        ],
      ),
    );
  }

  void navigateToReviewDetail(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => ProductReviewRatingScreen(
        product: widget.product,
      ),
    ));
  }

  void _addItemToCart(BuildContext context, ProductDetailsLoadedState state) async {
    if (state.productAttributes.selectedAttributes.length ==
        state.product?.selectableAttributes.length) {
      BlocProvider.of<ProductDetailsBloc>(context).add(ProductAddToCartEvent());
      await Navigator.pushNamed(context, AppRoutes.cart);
    } else {
      for (int i = 0; i < (state.product?.selectableAttributes.length ?? 0); i++) {
        final attribute = state.product?.selectableAttributes[i];
        if (!state.productAttributes.selectedAttributes
            .containsKey(attribute)) {
          _showSelectAttributeBottomSheet(context, attribute!,
              onSelect: i == 0
                  ? (() => {
                BlocProvider.of<ProductDetailsBloc>(context)
                    .add(ProductAddToCartEvent()),
                Navigator.pushNamed(
                    context, AppRoutes.cart)
              })
                  : null);
        }
      }
    }
  }
}
