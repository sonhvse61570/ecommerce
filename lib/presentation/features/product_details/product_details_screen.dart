import 'package:ecommerce/data/model/product_attribute.dart';
import 'package:ecommerce/presentation/features/product_details/views/details.dart';
import 'package:ecommerce/presentation/features/wrapper.dart';
import 'package:ecommerce/presentation/widgets/independent/scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'product_details_bloc.dart';
import 'product_details_event.dart';
import 'product_details_state.dart';

class ProductDetailsScreen extends StatefulWidget {
  const ProductDetailsScreen({Key? key}) : super(key: key);


  @override
  _ProductDetailsScreenState createState() => _ProductDetailsScreenState();
}

class ProductDetailsParameters {
  final int productId;
  final int categoryId;
  final Map<ProductAttribute, String>? selectedAttributes;

  const ProductDetailsParameters(this.productId, this.categoryId,
      {this.selectedAttributes});
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ProductDetailsParameters;
    print('productId: ${args.productId}');
    return SafeArea(
        child: OpenFlutterScaffold(
          background: null,
          title: '',
          body: BlocProvider<ProductDetailsBloc>(
            create: (context) {
              return ProductDetailsBloc(args.productId)
                ..add(ProductDetailsLoadedEvent(
                    productId: args.productId,
                    categoryId: args.categoryId));
            },
            child: _ProductWrapper(),
          ),
          bottomMenuIndex: 1,
        ));
  }
}

class _ProductWrapper extends StatefulWidget {
  @override
  _ProductWrapperState createState() => _ProductWrapperState();
}

class _ProductWrapperState extends OpenFlutterWrapperState<_ProductWrapper> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductDetailsBloc, ProductDetailsState>(
        bloc: BlocProvider.of<ProductDetailsBloc>(context),
        builder: (BuildContext context, ProductDetailsState state) {
          if (state is ProductDetailsLoadedState) {
            return ProductDetailsView(
                product: state.product!,
                similarProducts: state.similarProducts,
                changeView: changePage);
          }
          return Container();
        });
  }
}