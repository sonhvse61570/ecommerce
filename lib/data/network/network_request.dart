import 'dart:convert';

import 'package:ecommerce/config/server_addresses.dart';
import 'package:ecommerce/data/error/exceptions.dart';
import 'package:ecommerce/data/model/filter_rules.dart';
import 'package:ecommerce/data/model/sort_rules.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class NetworkRequest {
  static const STATUS_OK = 200;
  static const STATUS_CREATED = 201;

  final http.Client client;
  final RequestType type;
  final Uri address;
  final Map<String, dynamic> body;
  Map<String, String> headers;
  final List<int> listBody;
  final String plainBody;

  NetworkRequest(this.type, this.address,
      {required this.client,
      required this.body,
      required this.plainBody,
      required this.listBody,
      required this.headers});

  Future<http.Response> getResult() async {
    if (kDebugMode) {
      print('ADDRESS: $address');
    }
    if (kDebugMode) {
      print('listBody: ${jsonEncode(listBody)}');
    }
    if (kDebugMode) {
      print('plainBody: $plainBody');
    }
    if (kDebugMode) {
      print('body: ${jsonEncode(body)}');
    }
    http.Response response;
    headers;
    try {
      Uri uri = address; //Uri.parse(address);
      switch (type) {
        case RequestType.post:
          response = await client.post(
            uri,
            headers: headers,
            body: jsonEncode(body),
          );
          break;
        case RequestType.get:
          response = await client.get(uri, headers: headers);
          break;
        case RequestType.put:
          response = await client.put(uri,
              body: body, headers: headers);
          break;
        case RequestType.delete:
          response = await client.delete(uri, headers: headers);
          break;
      }
      if (kDebugMode) {
        print('RESULT: ${response.body}');
      }
      if (response.statusCode != STATUS_OK) {
        throw HttpRequestException();
      }
      return response;
    } catch (exception) {
      if (exception is HttpRequestException) {
        rethrow;
      } else {
        throw RemoteServerException();
      }
    }
  }

  factory NetworkRequest.productList(
      http.Client client,
      int pageIndex,
      int pageSize,
      int categoryId,
      FilterRules filterRules,
      SortRules sortRules) {
    List<String> parameters = [];
    parameters.add('page=${pageIndex + 1}');
    parameters.add('per_page=$pageSize');
    parameters.add('category=$categoryId');
    parameters.add('orderby=${sortRules.jsonRuleName}');
    parameters.add('order=${sortRules.jsonOrder}');
    //TODO add filter rules here
    Uri serverAddress =
        Uri(path: ServerAddresses.serverAddress + '?' + parameters.join('&'));
    return NetworkRequest(
      RequestType.get,
      serverAddress,
      client: client, body: {}, plainBody: '', listBody: [], headers: {},
    );
  }
}

enum RequestType {
  post,
  get,
  put,
  delete,
}
