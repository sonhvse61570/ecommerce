
import 'package:ecommerce/domain/entities/promo/promo_code_entity.dart';
import 'package:intl/intl.dart';

class PromoCodeModel extends PromoCodeEntity {
  const PromoCodeModel({
    required int id,
    required String title,
    required String promoCode,
    required double discountPercent,
    required DateTime dateExpires
  }) : super(
    id: id,
    title: title,
    promoCode: promoCode,
    discountPercent: discountPercent,
    dateExpires: dateExpires,
    belongsToUser: false,
    image: "",
    wasUsed: false
  );

  factory PromoCodeModel.fromJson(Map<String, dynamic> json) {
    List<String> images = [];
    if ( json['images']!=null ) {
      for (var f in (json['images'] as List)) {
        images.add(f['src']);
      }
    }
    return PromoCodeModel(
      id: (json['id'] as num).toInt(), 
      title: json['description'],
      promoCode: json['code'],
      discountPercent: double.parse(json['amount']),
      dateExpires: DateTime.parse(json['date_expires'])
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id, 
      'code': promoCode,
      'amount': discountPercent,
      'discount_type': 'percent',
      'description': title,
      'date_expires':DateFormat('yyyy-MM-ddTHH:mm:ss').format(dateExpires!)
    };
  }
}