import 'package:ecommerce/domain/usecases/products/products_by_filter_params.dart';

abstract class RemoteRepositoryWrapperAbstract {
  Future<List<dynamic>> getCategoryList({int parentId = 0});
  Future<List<dynamic>> getProductList(ProductsByFilterParams params);
  Future<List<dynamic>> getPromoList({int userId = 0});
}