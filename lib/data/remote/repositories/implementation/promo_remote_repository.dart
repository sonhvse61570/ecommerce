import 'package:ecommerce/data/model/promo.dart';
import 'package:ecommerce/data/remote/models/promo_code_model.dart';
import 'package:ecommerce/data/remote/repositories/abstract/remote_repository_wrapper_abstract.dart';
import 'package:ecommerce/data/repositories/abstract/promo_repository.dart';

class PromoRemoteRepository extends PromoRepository {
  final RemoteRepositoryWrapperAbstract ecommerce;

  PromoRemoteRepository(this.ecommerce);

  @override
  Future<List<Promo>> getPromoList() async {
    var promosData = await ecommerce.getPromoList();
    List<Promo> promos = [];
    for (var promo in promosData) {
      promos.add(Promo.fromEntity(PromoCodeModel.fromJson(promo)));
    }

    return promos;
  }

}