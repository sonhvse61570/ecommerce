import 'package:ecommerce/data/error/exceptions.dart';
import 'package:ecommerce/data/model/category.dart';
import 'package:ecommerce/data/remote/models/product_category_model.dart';
import 'package:ecommerce/data/remote/repositories/abstract/remote_repository_wrapper_abstract.dart';
import 'package:ecommerce/data/repositories/abstract/category_repository.dart';

class CategoryRemoteRepository extends CategoryRepository {
  final RemoteRepositoryWrapperAbstract ecommerce;

  CategoryRemoteRepository(this.ecommerce);

  @override
  Future<List<ProductCategory>> getCategories({int parentCategoryId = 0}) async {
    try {
      List<dynamic> categoriesData = await ecommerce.getCategoryList(parentId: parentCategoryId);
      List<ProductCategory> categories = [];

      for (var category in categoriesData) {
        categories.add(ProductCategory.fromEntity(ProductCategoryModel.fromJson(category)));
      }
      return categories;
    } on HttpRequestException {
      throw RemoteServerException();
    }
  }

  @override
  Future<ProductCategory?> getCategoryDetails(int categoryId) {
    return Future.value(null);
  }
}