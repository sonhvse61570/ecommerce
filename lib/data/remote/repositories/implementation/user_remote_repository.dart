import 'dart:convert';

import 'package:ecommerce/config/server_addresses.dart';
import 'package:ecommerce/data/model/app_user.dart';
import 'package:ecommerce/data/remote/utils.dart';
import 'package:ecommerce/data/repositories/abstract/user_repository.dart';
import 'package:http/http.dart';

class UserRemoteRepository extends UserRepository {
  @override
  Future<void> forgotPassword({required String email}) async {
    try {
      var route = HttpClient().createUri(ServerAddresses.forgotPassword);
      var data = <String, String>{
        'email': email,
      };

      var response = await post(route, body: data);
      Map jsonResponse = json.decode(response.body);
      if (response.statusCode != 200) {
        throw jsonResponse['message'];
      }
    } catch (error) {
      rethrow;
    }
  }

  @override
  Future<AppUser> getUser() async {
    try {
      // TODO api call for user information
      await Future.delayed(const Duration(seconds: 2));

      return AppUser(email: '', password: '', token: '');
    } catch (error) {
      rethrow;
    }
  }

  @override
  Future<String> signIn({required String email, required String password}) async {
    var route = HttpClient().createUri(ServerAddresses.authToken);
    var data = <String, String>{
      'username': email,
      'password': password,
    };

    var response = await post(route, body: data);
    Map jsonResponse = json.decode(response.body);
    if (response.statusCode != 200) {
      throw jsonResponse['message'];
    }
    return jsonResponse['token'];
  }

  @override
  Future<String> signUp({required String name, required String email, required String password}) async {
    try {
      var route = HttpClient().createUri(ServerAddresses.signUp);
      var data = <String, String>{
        'name': name,
        'username': email,
        'password': password,
      };

      var response = await post(route, body: data);
      Map jsonResponse = json.decode(response.body);
      if (response.statusCode != 200) {
        throw jsonResponse['message'];
      }
      return jsonResponse['token'];
    } catch (error) {
      rethrow;
    }
  }

}