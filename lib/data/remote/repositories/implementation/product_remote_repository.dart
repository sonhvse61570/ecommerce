import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/error/exceptions.dart';
import 'package:ecommerce/data/model/favorite_product.dart';
import 'package:ecommerce/data/model/filter_rules.dart';
import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/model/sort_rules.dart';
import 'package:ecommerce/data/remote/models/product_model.dart';
import 'package:ecommerce/data/remote/repositories/abstract/remote_repository_wrapper_abstract.dart';
import 'package:ecommerce/data/repositories/abstract/product_repository.dart';
import 'package:ecommerce/domain/usecases/products/products_by_filter_params.dart';

class ProductRemoteRepository extends ProductRepository {
  final RemoteRepositoryWrapperAbstract ecommerce;

  ProductRemoteRepository(this.ecommerce);

  @override
  Future<FilterRules?> getPossibleFilterOptions(int categoryId) {
    return Future.value(null);
  }

  @override
  Future<Product> getProduct(int id) {
    // TODO: implement getProduct
    throw UnimplementedError();
  }

  @override
  Future<List<Product>> getProducts(
      {int pageIndex = 0,
      int pageSize = AppConsts.pageSize,
      int categoryId = 0,
      SortRules sortRules = const SortRules(),
      FilterRules? filterRules}) async {
    try {
      List<dynamic> productsData =
          await ecommerce.getProductList(ProductsByFilterParams(
        categoryId: categoryId,
        sortBy: sortRules,
        filterRules: filterRules,
      ));

      List<Product> products = [];
      for (var product in productsData) {
        products.add(Product.fromEntity(ProductModel.fromJson(product)));
      }
      return products;
    } on HttpRequestException {
      throw RemoteServerException();
    }
  }

  @override
  Future<List<Product>> getSimilarProducts(int productId,
      {int pageIndex = 0, int pageSize = AppConsts.pageSize}) {
    // TODO: implement getSimilarProducts
    throw UnimplementedError();
  }
}
