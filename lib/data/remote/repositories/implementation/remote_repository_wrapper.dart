import 'dart:convert';

import 'package:ecommerce/config/server_addresses.dart';
import 'package:ecommerce/data/error/exceptions.dart';
import 'package:ecommerce/data/remote/repositories/abstract/remote_repository_wrapper_abstract.dart';
import 'package:ecommerce/domain/usecases/products/products_by_filter_params.dart';
import 'package:http/http.dart' as http;

class RemoteRepositoryWrapper implements RemoteRepositoryWrapperAbstract {
  final http.Client client;

  RemoteRepositoryWrapper(this.client);

  @override
  Future<List> getCategoryList({int parentId = 0}) {
    return _getApiRequest(ServerAddresses.productCategories);
  }

  @override
  Future<List> getProductList(ProductsByFilterParams params) {
    return _getApiRequest(ServerAddresses.products);
  }

  @override
  Future<List> getPromoList({int userId = 0}) {
    return _getApiRequest(ServerAddresses.promos);
  }

  Future<List<dynamic>> _getApiRequest(String url) async {
    final response = await client.get(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw HttpRequestException();
    }
  }
}
