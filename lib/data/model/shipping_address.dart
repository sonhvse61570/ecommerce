import 'package:ecommerce/domain/entities/checkout/shipping_address_entity.dart';

class ShippingAddressModel extends ShippingAddressEntity {
  const ShippingAddressModel(
    {required int id,
    required String fullName,
    required String address,
    required String city,
    required String state,
    required String postal,
    required country,
    required bool isDefault}
  ) : super(
      id: id,
      fullName: fullName,
      address: address,
      city: city,
      state: state,
      postal: postal,
      country: country,
      isDefault: isDefault);

  ShippingAddressModel copyWith(
    {int? id,
    String? fullName,
    String? address,
    String? city,
    String? state,
    String? postal,
    String? country,
    bool? isDefault})
  {
    return ShippingAddressModel(
      id: id ?? this.id,
      fullName: fullName ?? this.fullName,
      address: address ?? this.address,
      city: city ?? this.city,
      state: state ?? this.state,
      postal: postal ?? this.postal,
      country: country ?? this.country,
      isDefault: isDefault ?? this.isDefault
    );
  }
}
