import 'package:ecommerce/data/model/product_attribute.dart';
import 'package:equatable/equatable.dart';

import 'product.dart';

class CartItem extends Equatable {
  final Product product;
  final Map<ProductAttribute, String> selectedAttributes;
  final ProductQuantity productQuantity;

  double get price => productQuantity.quantity * product.price;

  const CartItem({
    required this.product,
    required this.productQuantity,
    required this.selectedAttributes,
  });

  void changeQuantity(int quantity){
    productQuantity.changeQuantity(quantity);
  }

  @override
  List<Object> get props => [product, selectedAttributes, productQuantity.quantity];
}

class ProductQuantity {
  int quantity;

  ProductQuantity(this.quantity);

  void changeQuantity(int newQuantity){
    quantity = newQuantity;
  }
}
