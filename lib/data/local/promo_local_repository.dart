
import 'package:ecommerce/data/model/promo.dart';
import 'package:ecommerce/data/repositories/abstract/promo_repository.dart';

class PromoLocalRepository implements PromoRepository {
  @override
  Future<List<Promo>> getPromoList() {
    return Future.value([]);
  }
  
}
