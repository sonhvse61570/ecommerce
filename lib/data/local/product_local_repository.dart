
import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/favorite_product.dart';
import 'package:ecommerce/data/model/filter_rules.dart';
import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/model/sort_rules.dart';
import 'package:ecommerce/data/repositories/abstract/product_repository.dart';

class ProductLocalRepository implements ProductRepository {
  @override
  Future<Product> getProduct(int id) {
    throw UnimplementedError();
  }

  @override
  Future<List<Product>> getSimilarProducts(int categoryId,
      {int pageIndex = 0, int pageSize = AppConsts.pageSize}) {
    throw UnimplementedError();
  }

  @override
  Future<List<Product>> getProducts(
      {int pageIndex = 0,
      int pageSize = AppConsts.pageSize,
      int categoryId = 0,
      bool isFavorite = false,
      SortRules sortRules = const SortRules(),
      required FilterRules? filterRules}) {
    return Future.value([]);
  }

  @override
  Future<FilterRules?> getPossibleFilterOptions(int categoryId) {
    return Future.value(null);
  }
}
