import 'package:ecommerce/data/local/database/data_source.dart';
import 'package:ecommerce/domain/entities/delivery/delivery_method_entity.dart';

class DeliveryMethodDataSource extends DataSource {

  @override
  Future<List<DeliveryMethodEntity>> all() async {
    checkDatabaseConnection();

    final List<Map<String, dynamic>> maps = await db!.query(tableName);

    return maps.map((e) => DeliveryMethodEntity(
      id: e['id'],
      title: e['title'],
      price: e['price'],
    )).toList();
  }

  @override
  Future<DeliveryMethodEntity> get(int id) async {
    checkDatabaseConnection();

    final List<Map<String, dynamic>> maps =
    await db!.query(tableName, where: '$primaryKey = ?', whereArgs: [id]);

    var method = maps[0];
    return DeliveryMethodEntity(
      id: method['id'],
      title: method['title'],
      price: method['price'],
    );
  }

  @override
  String get primaryKey => 'DeliveryMethod';

  @override
  String get tableName => 'id';

}