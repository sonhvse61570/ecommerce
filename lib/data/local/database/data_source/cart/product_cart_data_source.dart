import 'package:ecommerce/data/local/database/data_source.dart';
import 'package:ecommerce/domain/entities/cart/product_cart_entity.dart';
import 'package:ecommerce/domain/entities/entity.dart';

class ProductCartDataSource extends DataSource {
  @override
  Future<List<ProductCartEntity>> all() async {
    checkDatabaseConnection();

    final List<Map<String, dynamic>> maps = await db!.query(tableName);

    return maps.map((e) => ProductCartEntity(
      id: e['id'],
      productId: e['productId'],
      productCount: e['productCount'],
      totalPrice: e['totalPrice'],
    )).toList();
  }

  @override
  Future<ProductCartEntity> get(int id) async {
    checkDatabaseConnection();

    final List<Map<String, dynamic>> maps = await db!.query(
      tableName,
      where: '$primaryKey = ?',
      whereArgs: [id],
    );

    var item = maps[0];
    return ProductCartEntity(
      id: item['id'],
      productId: item['productId'],
      productCount: item['productCount'],
      totalPrice: item['totalPrice'],
    );
  }

  @override
  String get primaryKey => 'id';

  @override
  String get tableName => 'ProductCart';
}
