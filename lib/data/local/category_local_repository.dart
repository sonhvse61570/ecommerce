import 'package:ecommerce/data/model/category.dart';
import 'package:ecommerce/data/repositories/abstract/category_repository.dart';

class CategoryLocalRepository extends CategoryRepository {
  @override
  Future<List<ProductCategory>> getCategories(
      {int parentCategoryId = 0}) async {
    return Future.value([]);
  }

  @override
  Future<ProductCategory?> getCategoryDetails(int categoryId) {
    return Future.value(null);
  }
}
