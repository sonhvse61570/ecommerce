import 'package:ecommerce/data/error/exceptions.dart';
import 'package:ecommerce/data/local/category_local_repository.dart';
import 'package:ecommerce/data/model/category.dart';
import 'package:ecommerce/data/network/network_status.dart';
import 'package:ecommerce/data/remote/repositories/implementation/category_remote_repository.dart';
import 'package:ecommerce/data/repositories/abstract/category_repository.dart';
import 'package:ecommerce/locator.dart';

class CategoryRepositoryImpl extends CategoryRepository {

  @override
  Future<List<ProductCategory>> getCategories({int parentCategoryId = 0}) async {
    try {
      NetworkStatus networkStatus = sl();
      CategoryRepository categoryRepository;

      if (await networkStatus.isConnected) {
        categoryRepository = CategoryRemoteRepository(sl());
      } else {
        categoryRepository = CategoryLocalRepository();
      }

      return await categoryRepository.getCategories(parentCategoryId: parentCategoryId);
    } on HttpRequestException {
      throw RemoteServerException();
    }
  }

  @override
  Future<ProductCategory?> getCategoryDetails(int categoryId) async {
    List<ProductCategory> categories = await getCategories();
    return categories.isNotEmpty ? categories[0] : null;
  }

}