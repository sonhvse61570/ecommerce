import 'package:ecommerce/data/model/payment_method.dart';
import 'package:ecommerce/data/repositories/abstract/payment_method_repository.dart';
import 'package:ecommerce/data/repositories/mock/payment_method_mock_repository.dart';

class PaymentMethodRepositoryImpl implements PaymentMethodRepository {
  final PaymentMethodDataStorage _dataStorage;

  PaymentMethodRepositoryImpl(this._dataStorage);

  @override
  Future addPaymentMethod(PaymentMethodModel shippingAddress) {
    // TODO: implement addPaymentMethod
    throw UnimplementedError();
  }

  @override
  Future<PaymentMethodModel> getDefaultPaymentMethod() async {
    PaymentMethodModel paymentMethod = _dataStorage.paymentMethods.firstWhere((element) => element.isDefault);
    return paymentMethod;
  }

  @override
  Future<List<PaymentMethodModel>> getPaymentMethodList() async {
    if (_dataStorage.paymentMethods.isEmpty) {
      PaymentMethodMockRepository repo = PaymentMethodMockRepository();
      _dataStorage.paymentMethods = await repo.getPaymentMethodList();
    }

    return _dataStorage.paymentMethods;
  }

  @override
  Future removePaymentMethod(int paymentMethodId) async {
    _dataStorage.paymentMethods.removeWhere((paymentMethods) => paymentMethods.id == paymentMethodId);
  }

  @override
  Future setDefaultPaymentMethod(int paymentMethodId) async {
    List<PaymentMethodModel> paymentMethods = _dataStorage.paymentMethods;
    _dataStorage.paymentMethods.clear();
    for (var method in paymentMethods) {
      _dataStorage.paymentMethods.add(method.copyWith(isDefault: method.id == paymentMethodId));
    }
  }
}

class PaymentMethodDataStorage {
  List<PaymentMethodModel> paymentMethods;

  PaymentMethodDataStorage(this.paymentMethods);
}