import 'dart:collection';

import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/error/exceptions.dart';
import 'package:ecommerce/data/local/product_local_repository.dart';
import 'package:ecommerce/data/model/favorite_product.dart';
import 'package:ecommerce/data/model/filter_rules.dart';
import 'package:ecommerce/data/model/hashtag.dart';
import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/model/product_attribute.dart';
import 'package:ecommerce/data/model/sort_rules.dart';
import 'package:ecommerce/data/network/network_status.dart';
import 'package:ecommerce/data/remote/repositories/implementation/product_remote_repository.dart';
import 'package:ecommerce/data/repositories/abstract/favorites_repository.dart';
import 'package:ecommerce/data/repositories/abstract/product_repository.dart';
import 'package:ecommerce/locator.dart';

class ProductRepositoryImpl extends ProductRepository with FavoritesRepository {
  static final ProductDataStorage _dataStorage = ProductDataStorage();

  @override
  Future addToFavoritesWithAttrs(Product product,
      HashMap<ProductAttribute, String> selectedAttributes) async {
    _dataStorage.favProducts.add(FavoriteProduct(product, selectedAttributes));
  }

  @override
  bool checkFavorite(int productId) {
    bool isFavorite = false;
    for (var product in _dataStorage.favProducts) {
      if (product.product.id == productId) {
        isFavorite = true;
        break;
      }
    }
    return isFavorite;
  }

  @override
  Future<List<FavoriteProduct>> getFavoriteProducts(
      {int pageIndex = 0,
      int pageSize = AppConsts.pageSize,
      SortRules sortRules = const SortRules(),
      FilterRules? filterRules}) async {
    return _dataStorage.favProducts;
  }

  @override
  Future<FilterRules> getFavoritesFilterOptions() async {
    return FilterRules.getSelectableAttributes(_dataStorage.products);
  }

  @override
  Future<FilterRules?> getPossibleFilterOptions(int categoryId) async {
    HashMap<ProductAttribute, List<String>> result = HashMap();

    return FilterRules(
        categories: HashMap(),
        hashTags: [],
        selectedHashTags: HashMap<HashTag, bool>(),
        selectableAttributes: result,
        selectedPriceRange: PriceRange(10, 100));
  }

  @override
  Future<Product> getProduct(int id) {
    // TODO: implement getProduct
    throw UnimplementedError();
  }

  @override
  Future<List<Product>> getProducts(
      {int pageIndex = 0,
      int pageSize = AppConsts.pageSize,
      int categoryId = 0,
      SortRules sortRules = const SortRules(),
      FilterRules? filterRules}) async {
    try {
      NetworkStatus networkStatus = sl();
      ProductRepository productRepository;
      if (await networkStatus.isConnected) {
        productRepository = ProductRemoteRepository(sl());
      } else {
        productRepository = ProductLocalRepository();
      }

      List<Product> products =
          await productRepository.getProducts(filterRules: filterRules);

      //check favorites
      _dataStorage.products = [];
      for (var product in products) {
        _dataStorage.products.add(product.favorite(checkFavorite(product.id)));
      }

      return _dataStorage.products;
    } on HttpRequestException {
      throw RemoteServerException();
    }
  }

  @override
  Future<List<Product>> getSimilarProducts(int productId,
      {int pageIndex = 0, int pageSize = AppConsts.pageSize}) {
    // TODO: implement getSimilarProducts
    throw UnimplementedError();
  }

  @override
  Future<List<FavoriteProduct>> removeFromFavorites(
      int productId, Map<ProductAttribute, String> selectedAttributes) async {
    _dataStorage.favProducts.removeWhere((product) =>
        product.product.id == productId &&
        product.favoriteForm == selectedAttributes);
    return _dataStorage.favProducts;
  }

  @override
  Future addToFavorites(FavoriteProduct product) async {
    _dataStorage.favProducts.add(product);
  }
}

class ProductDataStorage {
  List<Product> products = [];
  List<FavoriteProduct> favProducts = [];
}
