import 'package:ecommerce/data/model/cart_item.dart';
import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/model/product_attribute.dart';
import 'package:ecommerce/data/model/promo.dart';
import 'package:ecommerce/data/repositories/abstract/cart_repository.dart';

class CartRepositoryImpl extends CartRepository {
  final CartProductDataStorage _cartProductDataStorage =
      CartProductDataStorage();

  @override
  Future addProductToCart(CartItem item) async {
    _cartProductDataStorage.items.add(item);
  }

  @override
  Future addProductToCartWithAttrs(Product product, int quantity,
      Map<ProductAttribute, String> selectedAttributes) async {
    _cartProductDataStorage.items.add(
      CartItem(
          product: product,
          productQuantity: ProductQuantity(quantity),
          selectedAttributes: selectedAttributes),
    );
  }

  @override
  Future changeQuantity(CartItem item, int newQuantity) async {
    for (var product in _cartProductDataStorage.items) {
      if (product == item) {
        product.productQuantity.changeQuantity(newQuantity);
      }
    }
  }

  @override
  Future<Promo?> getAppliedPromo() async {
    return _cartProductDataStorage.appliedPromo;
  }

  @override
  double getCalculatedPrice() {
    final totalPrice = getTotalPrice();
    final calculatedTotalPrice = _cartProductDataStorage.appliedPromo != null
        ? totalPrice *
            (1 - _cartProductDataStorage.appliedPromo!.discount / 100)
        : totalPrice;
    return calculatedTotalPrice;
  }

  @override
  double getTotalPrice() {
    return _cartProductDataStorage.items
        .fold(0, (previousValue, element) => previousValue + element.price);
  }

  @override
  Future setPromo(Promo? promo) async {
    _cartProductDataStorage.appliedPromo = promo;
  }

  @override
  Future<List<CartItem>> getCartContent() async {
    return _cartProductDataStorage.items;
  }

  @override
  Future removeProductFromCart(CartItem item) async {
    _cartProductDataStorage.items.remove(item);
  }
}

class CartProductDataStorage {
  List<CartItem> items = [];
  Promo? appliedPromo;
}
