import 'package:ecommerce/data/model/shipping_address.dart';
import 'package:ecommerce/data/repositories/abstract/shipping_address_repository.dart';
import 'package:ecommerce/data/repositories/mock/shipping_address_mock_repository.dart';

class ShippingAddressRepositoryImpl implements ShippingAddressRepository {
  final ShippingAddressDataStorage _dataStorage;

  ShippingAddressRepositoryImpl(this._dataStorage);

  @override
  Future addShippingAddress(ShippingAddressModel shippingAddress) async {
    _dataStorage.shippingAddresses.add(shippingAddress);
  }

  @override
  Future<ShippingAddressModel> getDefaultShippingAddress() async {
    ShippingAddressModel shippingAddress = _dataStorage.shippingAddresses.firstWhere((address) => address.isDefault);
    return shippingAddress;
  }

  @override
  Future<List<ShippingAddressModel>> getShippingAddressList() async {
    if (_dataStorage.shippingAddresses.isEmpty) {
      ShippingAddressMockRepository repo = ShippingAddressMockRepository();
      _dataStorage.shippingAddresses = await repo.getShippingAddressList();
    }
    return _dataStorage.shippingAddresses;
  }

  @override
  Future removeShippingAddress(int shippingAddressId) async {
    _dataStorage.shippingAddresses
        .removeWhere((address) => address.id == shippingAddressId);
  }

  @override
  Future setDefaultShippingAddress(int shippingAddressId) async {
    List<ShippingAddressModel> shippingAddresses =
        _dataStorage.shippingAddresses;
    _dataStorage.shippingAddresses.clear();
    for (var address in shippingAddresses) {
      _dataStorage.shippingAddresses.add(
        address.copyWith(isDefault: address.id == shippingAddressId),
      );
    }
  }
}

class ShippingAddressDataStorage {
  List<ShippingAddressModel> shippingAddresses;

  ShippingAddressDataStorage(this.shippingAddresses);
}
