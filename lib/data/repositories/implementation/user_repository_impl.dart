import 'package:ecommerce/data/model/app_user.dart';
import 'package:ecommerce/data/remote/repositories/implementation/user_remote_repository.dart';
import 'package:ecommerce/data/repositories/abstract/user_repository.dart';

class UserRepositoryImpl extends UserRepository {
  final UserRemoteRepository _userRemoteRepository;

  UserRepositoryImpl(this._userRemoteRepository);

  @override
  Future<void> forgotPassword({required String email}) => _userRemoteRepository.forgotPassword(email: email);

  @override
  Future<AppUser> getUser() {
    try {
      return _userRemoteRepository.getUser();
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<String> signIn({required String email, required String password}) =>
      _userRemoteRepository.signIn(email: email, password: password);

  @override
  Future<String> signUp(
          {required String name,
          required String email,
          required String password}) =>
      _userRemoteRepository.signUp(
          name: name, email: email, password: password);
}
