import 'package:ecommerce/data/error/exceptions.dart';
import 'package:ecommerce/data/local/promo_local_repository.dart';
import 'package:ecommerce/data/model/promo.dart';
import 'package:ecommerce/data/network/network_status.dart';
import 'package:ecommerce/data/remote/repositories/implementation/promo_remote_repository.dart';
import 'package:ecommerce/data/repositories/abstract/promo_repository.dart';
import 'package:ecommerce/locator.dart';

class PromoRepositoryImpl extends PromoRepository {
  static final PromoDataStorage _dataStorage = PromoDataStorage();

  @override
  Future<List<Promo>> getPromoList() async {
    try {
      NetworkStatus networkStatus = sl();
      PromoRepository promoRepository;

      if (await networkStatus.isConnected) {
        promoRepository = PromoRemoteRepository(sl());
      } else {
        promoRepository = PromoLocalRepository();
      }

      _dataStorage.items = await promoRepository.getPromoList();

      return _dataStorage.items;
    } on HttpRequestException {
      throw RemoteServerException();
    }
  }

}

class PromoDataStorage {
  List<Promo> items = [];
}