class PasswordMockRepository {
  Future<String> getCurrentPassword() async {
    return 'adminadmin';
  }

  Future<void> changePassword(String password) async {
    await Future.delayed(const Duration(seconds: 4));
  }
}
