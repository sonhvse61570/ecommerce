import 'dart:collection';

import 'package:ecommerce/config/app_theme.dart';
import 'package:ecommerce/data/model/favorite_product.dart';
import 'package:ecommerce/data/model/filter_rules.dart';
import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/model/product_attribute.dart';
import 'package:ecommerce/data/model/sort_rules.dart';

abstract class FavoritesRepository {
  ///returns list of products with selected properties, so the user can
  ///immediately add them to the cart without selecting anything else.
  ///The result is limited to [pageSize] and at [pageIndex] page.
  ///The result can be filtered by [filterRules] which basically consist of
  ///attributes, subcategories and price range, but can be extended to include more.
  ///The result can be sorted by [sortRules] with values coming from lowest to
  ///highest or vice versa with main sort parameter specified.
  Future<List<FavoriteProduct>> getFavoriteProducts({
    int pageIndex = 0,
    int pageSize = AppConsts.pageSize,
    SortRules sortRules = const SortRules(),
    FilterRules filterRules,
  });

  ///adds product with [productId] to the list of favorites. It is required
  ///that all selectable properties should be set so it will be easy to order
  ///it then
  Future addToFavoritesWithAttrs(
      Product product, HashMap<ProductAttribute, String> selectedAttributes);

  Future addToFavorites(FavoriteProduct product);

  ///removes product with [productId] from the list of favorites
  Future<List<FavoriteProduct>> removeFromFavorites(int productId, Map<ProductAttribute, String> selectedAttributes);

  //check if product was added to favorite
  bool checkFavorite(int productId);

  ///returns filter options available for favorite products.
  ///All rules should be set with initial (unselected) values
  Future<FilterRules> getFavoritesFilterOptions();
}
