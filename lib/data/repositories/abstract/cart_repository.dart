// Category repository
// Author: openflutterproject@gmail.com
// Date: 2020-02-06

import 'package:ecommerce/data/model/cart_item.dart';
import 'package:ecommerce/data/model/product.dart';
import 'package:ecommerce/data/model/product_attribute.dart';
import 'package:ecommerce/data/model/promo.dart';

abstract class CartRepository {
  ///returns all cart products with their quantity and attributes
  Future<List<CartItem>> getCartContent();

  ///adds product with selected attributes to cart
  Future addProductToCartWithAttrs(Product product, int quantity,
      Map<ProductAttribute, String> selectedAttributes);

  ///adds product to cart
  Future addProductToCart(CartItem item);

  ///removes product from cart
  Future removeProductFromCart(CartItem item);

  ///sets new quantity for the product in cart. If there is no such product
  ///in cart, does nothing. It can be used also to remove product from cart
  Future changeQuantity(CartItem item, int newQuantity);

  ///gets discount properties
  Future<Promo?> getAppliedPromo();

  ///applies promo to cart
  Future setPromo(Promo? promo);

  ///get cart total price
  double getTotalPrice();

  ///get calculated price with promo discount
  double getCalculatedPrice();
}
